/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.game;

import blackjack.model.Prop;
import java.util.List;
import blackjack.model.GameRules;
import static blackjack.model.GameRules.Group.AFFECT_STRATEGY;
import static blackjack.model.GameRules.Group.EDITABLE;
import static blackjack.model.GameRules.Group.MILLION;
import java.util.ArrayList;
import java.util.Arrays;
import static blackjack.model.GameRules.Group.RULE;
import static blackjack.model.GameRules.Group.STRATEGY_MENU;
import static blackjack.model.GameRules.Group.SURRENDER;
import blackjack.model.Strategy;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 * Collection of all game settings including unchangeable rules.
 * 
 * @author Dmitry Fazunenko
 */
class Rules implements GameRules {
    private final Prop shoeSizeProp = new Prop("Decks#", menuItems(ShoeSize.values()), ShoeSize.TWO.item(), RULE, EDITABLE, AFFECT_STRATEGY, MILLION);
    private final Prop soft17Prop = new Prop("Soft17", menuItems(Soft17.values()), Soft17.HIT.item(), RULE);
    private final Prop doublingProp = new Prop("Doubling", menuItems(Doubling.values()), Doubling.ANY_TWO.item(), RULE);
    private final Prop doubleAfterSlitProp = new Prop("DoubleAfter", menuItems(DoubleAfterSplit.values()), DoubleAfterSplit.ALLOWED.item(), RULE);
    private final Prop playerBJProp = new Prop("BJ", menuItems(PlayerBJ.values()), PlayerBJ.THREE_TO_TWO.item(), RULE);
    private final Prop dealerBJProp = new Prop("DealerBJ", menuItems(DealerBJ.values()), DealerBJ.PEEK.item(), RULE, EDITABLE, AFFECT_STRATEGY, MILLION);
    private final Prop insuranceProp = new Prop("Insurance", menuItems(InsuranceOption.values()), InsuranceOption.SUGGEST.item(), RULE, EDITABLE, MILLION);
    private final Prop surrenderProp = new Prop("Surrender", menuItems(SurrenderOption.values()), SurrenderOption.NO_SURRENDER.item(), AFFECT_STRATEGY, EDITABLE, SURRENDER);
    private final Prop betMethodProp = new Prop("BetMethod", menuItems(BetMethod.values()), BetMethod.PRO.item(), STRATEGY_MENU, MILLION);
    private final Prop strategyTypeProp = new Prop("Strategy", menuItems(StrategyType.values()), StrategyType.ADVANCED.item(), STRATEGY_MENU, AFFECT_STRATEGY, MILLION);

    // artificial property which depends on otrhers
    private final Prop surEarlyLateNoProp = new Prop("Sur", null, null, RULE, MILLION);

    private final List<Prop> all = Arrays.asList(
            shoeSizeProp, soft17Prop, doublingProp, doubleAfterSlitProp, dealerBJProp, surEarlyLateNoProp, 
            playerBJProp, surrenderProp, insuranceProp, 
            strategyTypeProp, betMethodProp
    );
    
    Rules(ObjectProperty<Strategy> strategy) {
        StrategyChangeListener listener = new StrategyChangeListener(strategy);
        for (Prop p: list(AFFECT_STRATEGY)) {
            p.value.addListener(listener);
        }
        strategy.set(getStrategy());
        
        ChangeListener surList = new SurrenderListener(surEarlyLateNoProp.value);
        surrenderProp.value.addListener(surList);
        dealerBJProp.value.addListener(surList);
        surList.changed(null, null, null);
    }
    @Override
    public List<Prop> list(Group... group) {
        List<Prop> selected = new ArrayList<>();
        all.stream().filter((p) -> (p.belongs(group))).forEachOrdered((p) -> {
            selected.add(p);
        });
        return selected;
    }
    
    Prop byName(String name) {
        for (Prop p: all) {
            if (name.equals(p.name)) {
                return p;
            }
        }
        return null;
    }
    
    void replecate(Rules rules) {
        rules.all.stream().forEach((p) -> {
            byName(p.name).value.set(p.value.get());
        });
    }
    
    int shoeSizeInDecks() {
         return ShoeSize.values()[shoeSizeProp.index()].size;
    }
    
    boolean suggestInsurance() {
        return InsuranceOption.values()[insuranceProp.index()].suggest;
    }
    
    boolean surrender() {
        return SurrenderOption.values()[surrenderProp.index()] == SurrenderOption.ALLOW_SURRENDER;
    }
    
    boolean useIndecis() {
        return StrategyType.values()[strategyTypeProp.index()] == StrategyType.ADVANCED;
    }
    
    boolean dealerPeeks() {
        return DealerBJ.values()[dealerBJProp.index()] == DealerBJ.PEEK;
    }
    
    
    BetMethod getBetMethod() {
        return BetMethod.values()[betMethodProp.index()];
    }
    
    Strategy getStrategy() {        
        return Strategies.byParams(shoeSizeInDecks(), useIndecis(), dealerPeeks(), surrender());
    }

    public enum ShoeSize implements Item {
        TWO(2), 
        SIX(6);
        private int size;
        ShoeSize(int size) {
            this.size = size;
        }
        @Override
        public String item() {
            return "#decks " + size;
        }
    };
    
    public enum Soft17 implements Item {
        HIT ("Soft17: dealer hits", true);
        private String item;
        public final boolean doHit;
        Soft17(String item, boolean doHit) {
            this.item = item;
            this.doHit = doHit;
        }
        @Override
        public String item() {
            return item;
        }    
    };

    public enum Doubling implements Item {
        ANY_TWO("Double: any two cards");
        private final String item;
        Doubling(String item) {
            this.item = item;
        }        
        @Override
        public String item() {
            return item;
        }
    }

    public enum DoubleAfterSplit implements Item {
        ALLOWED("Double after split: allowed");
        private final String item;
        DoubleAfterSplit(String item) {
            this.item = item;
        }        
        @Override
        public String item() {
            return item;
        }
    }
    
    public enum PlayerBJ implements Item {
        THREE_TO_TWO("BJ pays: 3 to 2");
        private String item;
        PlayerBJ(String item) {
            this.item = item;
        }        
        @Override
        public String item() {
            return item;
        }
    }

    public enum DealerBJ implements Item {
        PEEK("Dealer BJ: peek"), NO_PEEK("Dealer BJ: no peek") ;
        private String item;
        DealerBJ(String item) {
            this.item = item;
        }        
        @Override
        public String item() {
            return item;
        }
    }
    
    public enum BetMethod implements Item {
        MIN("Bet minimum"),
        PRO("Bet pro rata"),
        MAX("Bet maximum");
        private String item;
        BetMethod(String item) {
            this.item = item;
        }        
        @Override
        public String item() {
            return item;
        }
    }
    
    public enum StrategyType implements Item {
        BASIC("Basic"),
        ADVANCED("With indices");
        
        private final String item;
        
        StrategyType(String item) {
            this.item = item;
        }        
        @Override
        public String item() {
            return item;
        }
    }

    public enum InsuranceOption implements Item {
        SUGGEST("Suggest insurance", true),
        REJECT("Reject insurance", false);
        private final String item;
        private final boolean suggest;
        InsuranceOption(String item, boolean suggest) {
            this.item = item;
            this.suggest = suggest;
        }        
        @Override
        public String item() {
            return item;
        }
    }
    
    public enum SurrenderOption implements Item {
        NO_SURRENDER("No surrender"),
        ALLOW_SURRENDER("Allow surrender");
        private final String item;
        SurrenderOption(String item) {
            this.item = item;
        }        
        @Override
        public String item() {
            return item;
        }
    }
    
    interface Item {
        public String item();        
    }
    private static List<String> menuItems(Item[] values) {    
        List<String> items = new ArrayList<>();
        for (Item i: values) {
            items.add(i.item());
        }
        return items;
    }
    
    private class StrategyChangeListener implements ChangeListener<String> {
        final ObjectProperty<Strategy> strategy;
        StrategyChangeListener(ObjectProperty<Strategy> strategy) {
            this.strategy = strategy;
        }
        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
            strategy.set(getStrategy());
        }
    }

    private class SurrenderListener implements ChangeListener<String> {
        final StringProperty p;
        SurrenderListener(StringProperty p) {
            this.p = p;
        }
        @Override
        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
            if (!surrender()) {
                p.set("No surrender");
            } else {
                p.set(dealerPeeks() ? "Late surrender" : "Early surrender");
            }
        }
    }
    
    @Override
    public String asString() {
        StringBuilder sb = new StringBuilder();
        for (Prop p:all) {
            sb.append(p.name).append("='").append(p.value.get()).append("'; ");
        }
        return sb.toString().trim();
    }

    @Override
    public void fromString(String str) {
        for (String s:str.split(";")) {
            setProp(s.trim());
        }
    }
    
    private void setProp(String s) {
        try {
            // format name='abc' 
            int k = s.indexOf('=');
            Prop p = byName(s.substring(0, k));
            String v = s.substring(k + 1);
            v = v.substring(1, v.length() - 1); // chop "'"
            p.value.set(v);
        } catch (RuntimeException e) {
            System.out.println("Failed to parse rule " + s);
            throw e;
        }
    }

}
