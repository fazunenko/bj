/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.game;

import blackjack.model.DealAction;
import blackjack.model.GameRules;
import blackjack.model.Shoe;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Utility class to observe a shuffle.
 * This class is created for the sake of testability.
 * If flight recording is enabled the initial shoe, each user action,
 * and final shoe will be recorded and printed out for further analysis.
 */
public interface FlightRecorder {
    
    void rules(GameRules rules);
    void shuffleBegin(Shoe d);   
    void shuffleEnd(Shoe d);
    void betAction(int playedCards, int runningCount, long userTotal, int bet,  int hands);
    void dealAction(int playedCards, int runningCount, long userTotal, DealAction act);
    
    /**
     * Record user action and shoe.
     * For the case when flight recording is turned on.
     */    
    static final class ON implements FlightRecorder {                
        public static final String RULE_PREFIX = "RULES: ";
        public static final String BEFORE_PR = "SHOE_BEFORE_SHUFFLE: ";
        public static final String AFTER_PR = "SHOE_AFTER_SHUFFLE: ";
        
        private final PrintStream out;
        
        private File fff;
        
        public ON() {
            this(System.out);
        }
        public ON(File file) throws IOException {
            this(file == null ? System.out : new PrintStream(new FileOutputStream(file)));
            this.fff = file;
        }

        public ON(PrintStream ps) {
            out = ps;
        }
        @Override public void rules(GameRules rules) {
            out.println(RULE_PREFIX + rules.asString());
        }

        
        @Override
        public void shuffleBegin(Shoe d) {
            out.println(BEFORE_PR + d.toString());
        }

        @Override
        public void shuffleEnd(Shoe d) {
            out.println(AFTER_PR + d.toString());
        }
        
        @Override
        public void betAction(int playedCards, int runningCount, long userTotal, int bet, int hands) {
            out.println("  BET_" + bet + "_" + hands + "; rc = " + runningCount);
        }

        @Override
        public void dealAction(int playedCards, int runningCount, long userTotal, DealAction act) {
            out.println("    " + act + "; total=" + userTotal + "; rc = " + runningCount);
        }
        
        @Override
        public String toString() {
            return out.getClass().toString() + " file " + fff;
        }
    }
        
    /**
     * Implementation which does nothing.
     * For the case when flight recording is turned off.
     */
    static final class OFF implements FlightRecorder {
        @Override public void rules(GameRules rules) {}        
        @Override public void shuffleBegin(Shoe d) {}
        @Override public void shuffleEnd(Shoe d) {}
        @Override public void betAction(int playedCards, int runningCount, long userTotal, int bet, int hands) {}
        @Override public void dealAction(int playedCards, int runningCount, long userTotal, DealAction act) {}
    }
}
