/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.game;

import blackjack.model.Strategy;
import blackjack.model.Strategy.Act;
import blackjack.model.Strategy.Dealer;
import blackjack.model.Strategy.IndexType;
import blackjack.model.Strategy.Player;
import blackjack.model.Strategy.StrategyIndex;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Strategy factory.
 * This class represents strategy logics for the following rules:
 * <ul>
 * <li> Dealer peeks, No surrender
 * <li> Dealer doesn't peek, No surrender
 * <li> Dealer peeks, Late surrender
 * <li> Dealer doesn't peek, Early surrender
 * </ul>
 * Number of decks in the shoe is not used.
 * Each strategy goes with Indices, which might be applied if true count is used.
 * 
 * @author Dmitry Fazunenko
 */
class Strategies {

    /**
     * The very basic strategy for: 6 decks, No surrender, Dealer BJ peeks.
     */    
    private static final String[] BASIC = {
            "HHHHHHHHHH", // H7
            "HHHHHHHHHH", // H8
            "HDDDDHHHHH", // H9
            "DDDDDDDDHH", // H10
            "DDDDDDDDDD", // H11
            "HHSSSHHHHH", // H12
            "SSSSSHHHHH", // H13
            "SSSSSHHHHH", // H14
            "SSSSSHHHHH", // H15
            "SSSSSHHHHH", // H16
            "SSSSSSSSSS", // H17
            "SSSSSSSSSS", // H18
            "HHHDDHHHHH", // A2
            "HHHDDHHHHH", // A3
            "HHDDDHHHHH", // A4
            "HHDDDHHHHH", // A5
            "HDDDDHHHHH", // A6
            "SddddSSHHH", // A7
            "SSSSSSSSSS", // A8
            "SSSSSSSSSS", // S20
            "PPPPPPHHHH", // (2,2)
            "PPPPPPHHHH", // (3,3)
            "HHHPPHHHHH", // (4,4)
            "DDDDDDDDHH", // (5,5)
            "PPPPPHHHHH", // (6,6)
            "PPPPPPHHHH", // (7,7)
            "PPPPPPPPPP", // (8,8)
            "PPPPPSPPSS", // (9,9)
            "SSSSSSSSSS", // (T,T)
            "PPPPPPPPPP" // (A,A)
    };
        
    private static final List<Change> NO_PEEK_NO_SURRENDER = Arrays.asList(
            new Change(Player.H11, Dealer.DT, Act.Hit),
            new Change(Player.H11, Dealer.DA, Act.Hit),
            new Change(Player.P8,  Dealer.DT, Act.Hit),
            new Change(Player.P8,  Dealer.DA, Act.Hit),
            new Change(Player.PA,  Dealer.DA, Act.Hit)
    );
            
    /**
     * Case for late surrender (if dealer peeks).
     */
    private static final List<Change> PEEK_SURRENDER = Arrays.asList(
            new Change(Player.H15, Dealer.DT, Act.Surrender),
            new Change(Player.H15, Dealer.DA, Act.Surrender),
            new Change(Player.H16, Dealer.D9, Act.Surrender),
            new Change(Player.H16, Dealer.DT, Act.Surrender),
            new Change(Player.H16, Dealer.DA, Act.Surrender),
            new Change(Player.H17, Dealer.DA, Act.SurrenderStand),
            new Change(Player.P8,  Dealer.DA, Act.Surrender)
    );

    /**
     * Case for late surrender (if dealer peeks).
     */
    private static final List<Change> NO_PEEK_SURRENDER = Arrays.asList(
            new Change(Player.H7,  Dealer.DA, Act.Surrender),
            new Change(Player.H11, Dealer.DT, Act.Hit),
            new Change(Player.H11, Dealer.DA, Act.Hit),
            new Change(Player.H12,  Dealer.DA, Act.Surrender),
            new Change(Player.H13,  Dealer.DA, Act.Surrender),
            new Change(Player.H14, Dealer.DT, Act.Surrender),
            new Change(Player.H14, Dealer.DA, Act.Surrender),
            new Change(Player.H15, Dealer.DT, Act.Surrender),
            new Change(Player.H15, Dealer.DA, Act.Surrender),
            new Change(Player.H16, Dealer.D9, Act.Surrender),
            new Change(Player.H16, Dealer.DT, Act.Surrender),
            new Change(Player.H16, Dealer.DA, Act.Surrender),
            new Change(Player.H17, Dealer.DA, Act.SurrenderStand),
            
            new Change(Player.P3,  Dealer.DA, Act.Surrender), 
            new Change(Player.P6,  Dealer.DA, Act.Surrender), 
            new Change(Player.P7,  Dealer.DT, Act.Surrender), 
            new Change(Player.P7,  Dealer.DA, Act.Surrender), 
            new Change(Player.P8,  Dealer.DT, Act.Surrender), 
            new Change(Player.P8,  Dealer.DA, Act.Surrender),          
            new Change(Player.PA,  Dealer.DA, Act.Hit)
    );
    
    static final TableStrategy BASIC_STRATEGY = new TableStrategy("Basic stategy", BASIC);
    static final TableStrategy NO_PEEK_NO_SURRENDER_STRATEGY = 
            new TableStrategy("no peek/no surrender", BASIC, NO_PEEK_NO_SURRENDER);
    static final TableStrategy NO_PEEK_SURRENDER_STRATEGY =                                                           
            new TableStrategy("no peek/surrender", BASIC, NO_PEEK_SURRENDER);
    static final TableStrategy PEEK_SURRENDER_STRATEGY = 
            new TableStrategy("peek/surrender", BASIC, PEEK_SURRENDER);
    
    /**
     * List of indices based on true count for rules when dealer peeks.
     */
    private static final List<Index> INDICES_PEEK = Arrays.asList(
            // HiLo-3+6
            
            // Hard Hit/Stay
            new Index(Player.H12, Dealer.D2, IndexType.HARD, Act.Stand,  3, true),
            new Index(Player.H12, Dealer.D3, IndexType.HARD, Act.Stand,  2, true),
            new Index(Player.H12, Dealer.D4, IndexType.HARD, Act.Hit,    0, false),
            new Index(Player.H12, Dealer.D5, IndexType.HARD, Act.Hit,   -2, false),
            new Index(Player.H12, Dealer.D6, IndexType.HARD, Act.Hit,   -1, false),
            new Index(Player.H13, Dealer.D2, IndexType.HARD, Act.Hit,   -1, false),
            new Index(Player.H13, Dealer.D3, IndexType.HARD, Act.Hit,   -2, false),
            new Index(Player.A7,  Dealer.DA, IndexType.HARD, Act.Stand,  1, true),
            
            // Hard Double
            new Index(Player.H8,  Dealer.D5, IndexType.HARD_DOUBLE, Act.Double, 4, true),
            new Index(Player.H8,  Dealer.D6, IndexType.HARD_DOUBLE, Act.Double, 2, true),
            new Index(Player.H9,  Dealer.D2, IndexType.HARD_DOUBLE, Act.Double, 1, true),
            new Index(Player.H9,  Dealer.D3, IndexType.HARD_DOUBLE, Act.Hit,   -1, false),
            new Index(Player.H9,  Dealer.D4, IndexType.HARD_DOUBLE, Act.Hit,   -3, false),
            new Index(Player.H9,  Dealer.D7, IndexType.HARD_DOUBLE, Act.Double, 3, true),
            new Index(Player.H10, Dealer.D9, IndexType.HARD_DOUBLE, Act.Hit,   -2, false),
            // new Index(Player.H11, Dealer.DT, Act.Hit,    3, false),

            // Soft Double
            new Index(Player.A2, Dealer.D4, IndexType.SOFT_DOUBLE, Act.Double, 3, true),
            new Index(Player.A2, Dealer.D5, IndexType.SOFT_DOUBLE, Act.Hit,    0, false),
            new Index(Player.A2, Dealer.D6, IndexType.SOFT_DOUBLE, Act.Hit,   -2, false),
            new Index(Player.A3, Dealer.D4, IndexType.SOFT_DOUBLE, Act.Double, 2, true),
            new Index(Player.A3, Dealer.D5, IndexType.SOFT_DOUBLE, Act.Hit,   -2, false),
            new Index(Player.A4, Dealer.D4, IndexType.SOFT_DOUBLE, Act.Hit,    0, false),
            new Index(Player.A5, Dealer.D3, IndexType.SOFT_DOUBLE, Act.Double, 4, true),
            new Index(Player.A6, Dealer.D2, IndexType.SOFT_DOUBLE, Act.Double, 1, true),
            new Index(Player.A7, Dealer.D2, IndexType.SOFT_DOUBLE, Act.Double, 0, true),
            new Index(Player.A7, Dealer.D3, IndexType.SOFT_DOUBLE, Act.Hit,   -3, false),
            new Index(Player.A8, Dealer.D3, IndexType.SOFT_DOUBLE, Act.Double, 5, true),
            new Index(Player.A8, Dealer.D4, IndexType.SOFT_DOUBLE, Act.Double, 3, true),
            new Index(Player.A8, Dealer.D5, IndexType.SOFT_DOUBLE, Act.Double, 1, true),
            new Index(Player.A8, Dealer.D6, IndexType.SOFT_DOUBLE, Act.Double, 1, true),
            new Index(Player.A9, Dealer.D4, IndexType.SOFT_DOUBLE, Act.Double, 6, true),
            new Index(Player.A9, Dealer.D5, IndexType.SOFT_DOUBLE, Act.Double, 5, true),
            new Index(Player.A9, Dealer.D6, IndexType.SOFT_DOUBLE, Act.Double, 4, true),

            // Splits
            new Index(Player.P3, Dealer.D2, IndexType.SPLITS, Act.Hit,    0, false),
            new Index(Player.P4, Dealer.D4, IndexType.SPLITS, Act.Split,  3, true),
            new Index(Player.P4, Dealer.D5, IndexType.SPLITS, Act.Hit,   -1, false),
            new Index(Player.P4, Dealer.D6, IndexType.SPLITS, Act.Hit,   -2, false),
            new Index(Player.P6, Dealer.D2, IndexType.SPLITS, Act.Hit,   -2, false),
            new Index(Player.P9, Dealer.D2, IndexType.SPLITS, Act.Stand, -3, false),
            new Index(Player.P9, Dealer.D7, IndexType.SPLITS, Act.Split,  3, true),
            new Index(Player.PT, Dealer.D4, IndexType.SPLITS, Act.Split,  6, true),
            new Index(Player.PT, Dealer.D5, IndexType.SPLITS, Act.Split,  5, true),
            new Index(Player.PT, Dealer.D6, IndexType.SPLITS, Act.Split,  4, true)
    );
    
    /**
     * Extention when dealer no peek.
     */
    private static final List<Index> EXT_INDICES_NO_PEEK = Arrays.asList(
        new Index(Player.H11, Dealer.DT, IndexType.HARD_DOUBLE, Act.Hit, 3, false)
    );

    /**
     * List of indices based on true count for surrender.
     */
    private static final List<Index> EXT_INDICES_NO_SURRENDER = Arrays.asList(
            new Index(Player.H15, Dealer.DT, IndexType.HARD, Act.Stand,  4, true),
            new Index(Player.H16, Dealer.D9, IndexType.HARD, Act.Stand,  4, true),
            new Index(Player.H16, Dealer.DT, IndexType.HARD, Act.Hit,    0, false)
    );            
    
    /**
     * List of indices based on true count for surrender.
     */
    private static final List<Index> INDICES_SURRENDER = Arrays.asList(
            // Surrender
            new Index(Player.H13, Dealer.D9, IndexType.SURRENDER, Act.Surrender, 12, true),
            new Index(Player.H14, Dealer.D8, IndexType.SURRENDER, Act.Surrender, 12, true),
            new Index(Player.H14, Dealer.D9, IndexType.SURRENDER, Act.Surrender,  6, true),
            new Index(Player.H15, Dealer.D7, IndexType.SURRENDER, Act.Surrender, 10, true),
            new Index(Player.H15, Dealer.D8, IndexType.SURRENDER, Act.Surrender,  6, true),
            new Index(Player.H15, Dealer.D9, IndexType.SURRENDER, Act.Surrender,  2, true),
            new Index(Player.H16, Dealer.D7, IndexType.SURRENDER, Act.Surrender,  9, true),
            new Index(Player.H16, Dealer.D8, IndexType.SURRENDER, Act.Surrender,  4, true),
            new Index(Player.H16, Dealer.D9, IndexType.SURRENDER, Act.Hit,       -1, false),
            new Index(Player.H17, Dealer.D9, IndexType.SURRENDER, Act.Surrender, 12, true)
    );
    
    private static final List<Index> EXT_INDICES_SURRENDER_NO_PEEK = Arrays.asList(
            new Index(Player.H12, Dealer.DT, IndexType.SURRENDER, Act.Surrender,  7, true),
            new Index(Player.H13, Dealer.DT, IndexType.SURRENDER, Act.Surrender,  3, true),
            new Index(Player.H14, Dealer.DT, IndexType.SURRENDER, Act.Surrender, -1, true),
            new Index(Player.H15, Dealer.DT, IndexType.SURRENDER, Act.Hit,       -3, false),
            new Index(Player.H16, Dealer.DT, IndexType.SURRENDER, Act.Stand,     -7, false),
            new Index(Player.H17, Dealer.DT, IndexType.SURRENDER, Act.Surrender,  6, true)            
    );
    
    private static final List<Index> PEEK_NO_SURRENDER_INDICES = peekNoSurIndices();
    private static final List<Index> NO_PEEK_NO_SURRENDER_INDICES = noPeekNoSurIndices();
    private static final List<Index> PEEK_SURRENDER_INDICES = peekSurrenderIndices();
    private static final List<Index> NO_PEEK_SURRENDER_INDICES = noPeekSurrenderIndices();

    private static List<Index> peekNoSurIndices() {
        List<Index> list = new ArrayList<>();
        list.addAll(INDICES_PEEK);
        list.addAll(EXT_INDICES_NO_SURRENDER);        
        return list;
    }
    
    private static List<Index> noPeekNoSurIndices() {
        List<Index> list = new ArrayList<>();
        list.addAll(INDICES_PEEK);
        list.addAll(EXT_INDICES_NO_PEEK);
        list.addAll(EXT_INDICES_NO_SURRENDER);        
        return list;
    }
    private static List<Index> noPeekSurrenderIndices() {
        List<Index> list = new ArrayList<>();
        list.addAll(INDICES_PEEK);
        list.addAll(EXT_INDICES_NO_PEEK);
        list.addAll(INDICES_SURRENDER);
        list.addAll(EXT_INDICES_SURRENDER_NO_PEEK);
        return list;
    }
    private static List<Index> peekSurrenderIndices() {
        List<Index> list = new ArrayList<>();
        list.addAll(INDICES_PEEK);
        list.addAll(INDICES_SURRENDER);
        return list;
    }
    
    /**
     * Creates a Strategy object by given rules.
     * @param decks number of decks (currently not used)
     * @param useIndices true, if indices are in use
     * @param dealerPeeks true, if dealer peeks when see Black Jack
     * @param surrender true, if surrender is allowed.
     * @return 
     */
    public static Strategy byParams(int decks, boolean useIndices, boolean dealerPeeks, boolean surrender) {
        TableStrategy basic;
        List<Index> indices;
        String rules;
        if (dealerPeeks) {
            if (surrender) {
                basic   = PEEK_SURRENDER_STRATEGY;
                indices = PEEK_SURRENDER_INDICES;
                rules = "Dealer BJ: peek; Late surrender";
            } else {
                basic   = BASIC_STRATEGY;
                indices = PEEK_NO_SURRENDER_INDICES;
                rules = "Dealer BJ: peek; No surrender";
            }
        } else {
            if (surrender) {
                basic   = NO_PEEK_SURRENDER_STRATEGY;
                indices = NO_PEEK_SURRENDER_INDICES;
                rules = "Dealer BJ: no peek; Early surrender";
            } else {
                basic   = NO_PEEK_NO_SURRENDER_STRATEGY;
                indices = NO_PEEK_NO_SURRENDER_INDICES;
                rules = "Dealer BJ: no peek; No surrender";
            }
        }
        return new ExtendedStrategy(rules, basic, indices, useIndices);
    }
            
    /**
     * Strategy which doesn't depend on true count.
     */
    public static abstract class BasicStrategy implements Strategy {
        public abstract Act action(Dealer dealer, Player player);
    
        @Override
        public final Acts actions(Dealer dealer, Player player) {
            return new Acts(action(dealer, player));
        }                  
    }
    
    /**
     *
     * @author Fa
     */
    static class TableStrategy extends BasicStrategy {

        Act[][] table = new Act[Strategy.Player.values().length][Strategy.Dealer.values().length];
        private final String name;

        private TableStrategy(String name, String str) {
            this(name, str.split(" "));
        }
        private TableStrategy(String name, String[] rows, List<Change> corrections) {
            this(name, rows);
            for (Change c: corrections) {                
                table[c.player.ordinal()][c.dealer.ordinal()] = c.act;
            }
        }

        private TableStrategy(String name, String... rows) {
            this.name = name;            
            if (rows.length != table.length) {
                throw new Error("Illegal number of rows " + rows.length + " expected: " + table.length);
            }
            for (int i = 0; i < rows.length; i++) {
                String row = rows[i].trim();
                if (row.length() != table[i].length) {
                    throw new Error("Illegal row '" + row + "' expected legnth: " + table[i].length);
                }
                for (int j = 0; j < row.length(); j++) {
                    table[i][j] = act(row.charAt(j));
                }
            }
        }
        
        private Act act(char c) {
            switch (c) {
                case 'H':
                    return Act.Hit;
                case 'S':
                    return Act.Stand;
                case 'P':
                    return Act.Split;
                case 'D':
                    return Act.Double;
                case 'd':
                    return Act.DoubleStand;
                case 'R':
                    return Act.Surrender;
                case 'r':
                    return Act.SurrenderStand;
                default:
                    throw new Error("Unknown char " + c);
            }
        }
        
        @Override
        public final Act action(Dealer dealer, Player player) {
            return table[player.ordinal()][dealer.ordinal()];
        }

        @Override
        public List<StrategyIndex> getIndices() {
            return null;
        }
        
        @Override
        public boolean useIndices() {
            return false;
        }
        
        @Override
        public String toString() {
            return name;
        }

        @Override
        public Strategy getBasicStrategy() {
            return this;
        }

    }

    static class ExtendedStrategy implements Strategy {

        private final Set<Index> corrections = new HashSet<>();
        private final List<StrategyIndex> indices = new ArrayList<>();
        private final BasicStrategy parent;
        private final String name;
        private final boolean useIndices;

        ExtendedStrategy(String name, BasicStrategy parent, List<Index> indices, boolean useIndices) {
            this.name = name;
            this.parent = parent;
            this.useIndices = useIndices;
            for (Index c : indices) {
                add(c);
            }
        }

        final void add(Index c) {
            indices.add(c);
            Index existing = search(c.dealer, c.player);
            if (existing != null) {
                throw new Error("Duplicated cells: old = " + existing + " new = " + c);
            }
            corrections.add(c);
        }

        private Index search(Dealer dealer, Player player) {
            for (Index c : corrections) {
                if (c.dealer == dealer && c.player == player) {
                    return c;
                }
            }
            return null;
        }

        /**
         * Default implementation ignores true count.
         * @return the same as actions(dealer, player)
         */
        @Override
        public Acts actions(Dealer dealer, Player player) {
            if (!useIndices) {
                return parent.actions(dealer, player);
            }
            Index correction = search(dealer, player);
            if (correction == null) {
                return parent.actions(dealer, player);
            }
            Act aBasic = parent.action(dealer, player);
            Act aCorrected = correction.act;
            if (correction.isAgressive) {
                return new Acts(aBasic, aCorrected, correction.tcLimit);
            } else {
                return new Acts(aCorrected, aBasic, correction.tcLimit);
            }             
        }        

        @Override
        public List<StrategyIndex> getIndices() {
            return indices;
        }
        @Override
        public boolean useIndices() {
            return useIndices;
        }
        
        
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public Strategy getBasicStrategy() {
            return parent;
        }
    }
    
    /**
     * Defines a single correction to the basic strategy.
     */
    static class Change {
        private final Strategy.Dealer dealer;
        private final Strategy.Player player;
        private final Strategy.Act act;
        private Change(Player player, Dealer dealer, Act act) {
            this.dealer = dealer;
            this.player = player;
            this.act = act;
        }
    }
 
    /**
     * Defines strategy correction for the dealer/player case.
     */
    static class Index implements StrategyIndex {
        
        private final Strategy.Dealer dealer;
        private final Strategy.Player player;
        private final Strategy.Act act;
        private final int tcLimit;        
        private final boolean isAgressive;
        private final IndexType type;
        
        private Index(Player player, Dealer dealer, IndexType type, Act act, int tc, boolean isAgressive) {
            this.dealer = dealer;
            this.player = player;
            this.act = act;
            this.tcLimit = tc;
            this.isAgressive = isAgressive;
            this.type = type;
        }
        
        public int getX() {
            return dealer.ordinal();
        }
        public int getY() {
            return player.ordinal();
        }
        public int getXY() {
            return getY() * 100 + getX();
        }
        
        @Override
        public String toString() {
            return player + "/" + dealer + "-->" + act;
        }

        @Override
        public Strategy.IndexType type() {
            return type;
        }

        @Override
        public Player player() {
            return player;
        }

        @Override
        public Dealer dealer() {
            return dealer;
        }

        @Override
        public int trueCount() {
            return tcLimit;
        }
    }
    
}