/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.game;

import blackjack.model.Shoe;
import blackjack.model.Card;
import blackjack.model.DealAction;
import blackjack.model.DealModel;
import blackjack.model.DealResult;
import blackjack.model.HandModel;
import blackjack.model.Strategy;
import blackjack.model.Strategy.Act;
import blackjack.model.Strategy.Acts;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Class that implements Deal logic.
 * No need to create instance for each single deal. It's expected that
 * method begin() will put all the properties into initial state.
 */
public class Deal implements DealModel {
    private final Hand dealer;
    private final ObjectProperty<Strategy> strategy;
    private final ObservableList<Hand> player;
    private final ObservableList<DealAction> availableActions;
    private final ObjectProperty<DealAction> optimalAction;
    private DealAction secondOptimalAction; // tolerant to small errors in trueCount
    private final IntegerProperty currHandNumber;    
    private final Shoe shoe;    
    private int bet = 10;
    private DealResult dealResult;
    private boolean suggestInsurance = false;
    private boolean allowSurrender = false;

    public Deal(Shoe shoe, ObjectProperty<Strategy> strategy) {
        this.shoe = shoe;
        this.strategy = strategy;
        dealer = new Hand();
        player = FXCollections.observableArrayList();
        availableActions = FXCollections.observableArrayList();
        currHandNumber = new SimpleIntegerProperty(-1);
        optimalAction = new SimpleObjectProperty<>();
    }
    
    public void begin(int bet, int hands) {
        if (hands < 1 || hands > 5) {
            throw new Error("Illegal number of hands " + hands);
        }
        clean();
        this.bet = bet;
        dealResult = null;        
        for (int i = 0; i < hands; i++) {
            Hand h = new Hand();
            h.add(shoe.nextCard());
            h.setNumber(i);
            h.setAllowSurrender(allowSurrender);
            player.add(i, h);
        }
        Card dealerOpen = shoe.nextCard();        
        dealer.setInsurance(suggestInsurance && dealerOpen.isAce);
        dealer.add(dealerOpen);
        for (int i = 0; i < hands; i++) {
            Hand h = player.get(i);
            h.add(shoe.nextCard());
        }
        dealer.add(shoe.nextClosedCard());
        
        currHandNumber.set(0);
        update();
    }
    
    void update() {
        updateCurrent();
        detectAvailableActions();
        detectOptimalAction();
    }

    public void end() {
        // open the closed card
        dealer.openClosedCards();
        shoe.endDeal();
        
        if (checkPlayerHands()) {
            while (dealer.points() < 17 || dealer.points() == 17 && dealer.softPoints() < 17) {
                dealer.add(shoe.nextCard());
            }
        }
        calcPlayerHands();
        dealer.result().setValue(-result().totalWin);
    }
    
    public void clean() {
        dealer.cards().clear();
        dealer.result().setValue(Integer.MAX_VALUE);
        dealer.start();
        player.clear();
    }
    
    /**
     * @return when there is a user hand which could win or lose.
     */
    private boolean checkPlayerHands() {
        for (Hand h: player) {
            if (!h.isBlackJack() && h.points() <= 21) {
                return true;
            }
        }
        return false;
    }
    
    
    /**
     * Sets totalWin and totalBet
     */
    void calcPlayerHands() {
        int totalWin = 0;
        int totalBet = 0;
        boolean isBJ = dealer.isBlackJack();
        int p = dealer.points();
        for (Hand h: player) {
            int insuranceWin = h.getInsuranceWin();
            totalWin += h.calcHand(isBJ, dealer.points(), bet);
            totalBet += h.isDouble() ? 2*bet : bet;            
            if (insuranceWin > 0) {
                totalBet += insuranceWin / 2;
            } else if (insuranceWin < 0) {
                totalBet += -insuranceWin;
            }
        }
        dealResult = new DealResult(totalWin, totalBet);
    }
    
    void setInsuranceOpt(boolean suggest) {
        suggestInsurance = suggest;
    }
    void allowSurrender(boolean available) {
        allowSurrender = available;
    }

    @Override
    public DealResult result() {
        if (dealResult == null) {
            throw new Error("Deal is not finished yet");
        }
        return dealResult;
    }
    
    public void handle(DealAction act) {
        if (!availableActions.contains(act)) {
            throw new Error("Illegal action " + act);
        }
        dealer.setInsurance(false);
        if (null != act) switch (act) {
            case SPLIT:
                doSplit();
                break;
            case HIT:
                doHit();
                break;
            case STAND:
                doStand();
                break;
            case DOUBLE:
                doDouble();
                break;
            case INSURANCE_YES:
                doInsuranceYes();
                break;
            case INSURANCE_NO:
                doInsuranceNo();
                break;
            case SURRENDER:
                doSurrender();
                break;
            default:
                break;
        }
        update();
    }
    
    private Hand current() {
        int n = currHandNumber.get();
        return n < 0 ? null : player.get(n);
    }
    
    private void updateCurrent() {
        
        if (dealer.isInsurance()) {
            return;
        }
        if (dealer.isBlackJack()) {
            currHandNumber.set(-1);
            return;
        } 
        if (current() == null || !current().isFinal()) {
            return;
        }
        for (int i = 0; i < player.size(); i++) {
            if (!player.get(i).isFinal()) {
                currHandNumber.set(i);
                player.get(i).openClosedCards();
                if (!player.get(i).isFinal()) {
                    return;
                }
            }
        }
        currHandNumber.set(-1);
    }
    
    @Override
    public ObservableList<DealAction> availableActions() {
        return availableActions;
    }
    
    private void detectAvailableActions() {
        availableActions.clear();        
        if (dealer.isInsurance()) {
            availableActions.add(DealAction.INSURANCE_YES);
            availableActions.add(DealAction.INSURANCE_NO);
            return;
        }
        
        Hand current = current();
        if (current == null || current.isFinal() || dealer.isBlackJack()) {
            return;
        }
        if (current.points() >= 12) {
            availableActions.add(DealAction.STAND);
        }
        if (!current.isAceSplit()) {
            availableActions.add(DealAction.HIT);
            if (current.isDoubleable()) {
                availableActions.add(DealAction.DOUBLE);
            }
        }                
        if (current.isSplitable()) {
            availableActions.add(DealAction.SPLIT);
        }         
        if (current().isSurrenderable()) {
            availableActions.add(DealAction.SURRENDER);
        }        
    }
    private void detectOptimalAction() {
        optimalAction.set(null);
        secondOptimalAction = null;
        Strategy s = strategy.get();
        if (availableActions.isEmpty() || s == null) {
            return;
        }
        if (availableActions.contains(DealAction.INSURANCE_YES)) {            
            optimalAction.set(shoe.getTrueCount() > 3 ? DealAction.INSURANCE_YES : DealAction.INSURANCE_NO);
            if (3 < shoe.getTrueCount() && shoe.getTrueCount() < 3.5) {
                secondOptimalAction = DealAction.INSURANCE_NO;
            } else if (2.5 < shoe.getTrueCount() && shoe.getTrueCount() <= 3) {
                secondOptimalAction = DealAction.INSURANCE_YES;
            }
            return;
        }
        Strategy.Player p = playerState();
        Strategy.Dealer d = dealerState();
        Acts variants = s.actions(d, p);
        Act optimal = variants.choose(shoe.getTrueCount());
        Act alt1 = variants.choose(shoe.getTrueCount() + 0.5f);
        Act alt2 = variants.choose(shoe.getTrueCount() - 0.5f);

        optimalAction.set(current().action(optimal));
        if (alt1 != optimal) {
            secondOptimalAction = current().action(alt1);
        } else if (alt2 != optimal) {
            secondOptimalAction = current().action(alt2);
        }
    }

    String verifyDealAction(DealAction act) {
        DealAction optimal = optimalAction().get();
        if (act == optimal || act == secondOptimalAction) {
            // last action is correct
            return null;
        }
        switch (optimal) {
            case INSURANCE_NO: 
            case INSURANCE_YES: return "Insurance:\n No(<=3) ; Yes(>3)";
            default: {
                Strategy s = strategy.get();
                Strategy.Player p = playerState();
                Strategy.Dealer d = dealerState();
                Acts variants = s.actions(d, p);
                return p + " vs " + d + "\n" + variants.toString();
            }
        }        
    }
    
    @Override
    public Strategy.Dealer dealerState() {
        return dealer.dealerState();
    }
    
    @Override
    public Strategy.Player playerState() {         
        return current() == null ? null : current().playerState();
    }
    
            
    private void doSplit() {
        Hand current = current();
        current.setHandObtainedFromSplit(true);
        Card c = current.remove();        
        if (c.isAce) {
            current.setAceSplit(true);
        }
        
        Hand newHand = new Hand().add(c);
        if (c.isAce) {
            newHand.setAceSplit(true);
        }
        newHand.setHandObtainedFromSplit(true);
        newHand.setNumber(current.number() + 1);
        for (int i = current.number() + 1; i < player.size(); i++) {
            player.get(i).shift();
        }
        player.add(newHand.number(), newHand);
        current.add(shoe.nextCard());
        
        newHand.add(c.isAce ? shoe.nextCard() : shoe.nextClosedCard());
    }
    
    private void doHit() {
        current().add(shoe.nextCard());
    }
    
    private void doStand() {
        current().stop();
    }
    
    private void doDouble() {
        current().setDouble().add(shoe.nextCard()).stop();
    }
    
    private void doInsuranceYes() {
        if (dealer.isBlackJack()) {
            for (Hand h:player) {
                h.setInsuranceWin(bet);
            }
        } else {
            for (Hand h:player) {
                h.setInsuranceWin(-bet/2);
            }
        }
    }     
    
    private void doInsuranceNo() {
        // no special action
    }
    
    private void doSurrender() {
        current().setSurrender();
    }     
        
    public void print() {
            System.out.println("dealer " + dealer);
            System.out.println("player " + player);
            System.out.println("---------------------");
    }
    
    StringProperty lastBid() {
        Hand h = current();
        return h == null ? null : h.lastBid();
    }
    
    @Override
    public HandModel dealerHand() {
        return dealer;
    }

    @Override
    public ObservableList<? extends HandModel> playerHands() {
        return player;
    }

    @Override
    public IntegerProperty currentHandNum() {
        return currHandNumber;
    }
    
    @Override
    public ObjectProperty<DealAction> optimalAction() {
        return optimalAction;
    }
}
    
