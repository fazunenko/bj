/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.game;

import blackjack.game.Rules.BetMethod;
import blackjack.game.Rules.ShoeSize;
import blackjack.model.Shoe;
import blackjack.model.GameState;
import blackjack.model.DealAction;
import blackjack.model.DealModel;
import blackjack.model.DealResult;
import blackjack.model.GameModel;
import blackjack.model.GameRules;
import blackjack.model.GameStatistics;
import blackjack.model.Strategy;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;

/**
 * The main class.
 * 
 * @author Dmitry Fazunenko
 */
public class Game implements GameModel {
    private final ObjectProperty<GameState> gameState = new SimpleObjectProperty<>();
    private final ObjectProperty<Strategy> strategy = new SimpleObjectProperty<>();
    private final StatProperty statistics = new StatProperty();

    private final Shoe shoe = new Shoe();
    private final Deal deal = new Deal(shoe, strategy);
    private final Rules rules = new Rules(strategy);
        
    private boolean isAutoplay = false;

    @Override
    public boolean isAutoPlay() {
        return isAutoplay;
    }

    @Override
    public ObjectProperty<GameState> getGameState() {
        return gameState;
    }
    
    @Override
    public GameRules getRules() {
        return rules;
    }
    

    @Override
    public ObjectProperty<Strategy> getStrategy() {
        return strategy;
    }
    
    @Override
    public void begin() {
        //int ddd[] = {2,3,9,9+13,0, 0, 9+26,9+39,2,16,30,44,5};
        //int ddd[] = {10, 10, 0, 0, 6, 15, 23, 23};
        //shoe.setShuffledCards("2h 3d 4d 5s 6h 7c 8d 9s Tc Jd Qs Kd As");
        //shoe.setShuffledCards("Ts 6d Td Ah 3d 6s 5c Ad 2d Qd Ks 7c 6c 3c Jc 8c 3h 5c 8h 9s 7h Jh Qs 4h Js 6h 7s 2h Ah 9h Qc 2s 5d 4c Kd Kc 9d Kh 8d Ts 7d 3s As 2c Td Ac 5h 3d Th 8s Jd 9c ");
        //shoe.setShuffledCards("5h 2d 5h Ad Ad 6d Ts As Ac 3h");
        if (gameState.get() != null) {
            throw new Error("Game alredy in progress");            
        }
        statistics.set(new GameStatistics());
        gameState.set(GameState.SHUFFLE);
    }
    
    @Override
    public void end() {
        deal.clean();
        gameState.set(null);
    }
    
    @Override
    public void userAutoPlay(boolean on) {
        if (gameState.get() == GameState.HANDLE_USER_ACTION) {
            throw new Error("Incorrect GameState for autoplaying: " + gameState.get());  
        }
        GameState currentState = gameState.get();
        gameState.set(GameState.HANDLE_USER_ACTION);
        isAutoplay = on;
        gameState.set(currentState);
    }

    @Override    
    public void userShuffle() {
        if (gameState.get() != GameState.SHUFFLE) {
            throw new Error("Incorrect GameState for shuffling: " + gameState.get());  
        }
        int decks = rules.shoeSizeInDecks();
        if (decks < 1 || decks > 8) {
           throw new Error("Illegal number of decks " + decks);
        }
        gameState.set(GameState.HANDLE_USER_ACTION);
        statistics.get().newShuffle();
        //shoeSizeProp.value.set(ShoeSize.values()[decks - 1].item());        
        shoe.shuffle(rules.shoeSizeInDecks());
        deal.clean();
        gameState.set(GameState.BET);
    }

    @Override
    public void userBet(int bet, int hands) {
        if (gameState.get() != GameState.BET) {
            throw new Error("Incorrect GameState for bettting: " + gameState.get());  
        }
        gameState.set(GameState.HANDLE_USER_ACTION);        
        deal.setInsuranceOpt(rules.suggestInsurance());
        deal.allowSurrender(rules.surrender());
        deal.begin(bet, hands);
        checkDeal();
    }
    
    @Override
    public void userEndShuffle() {
        if (gameState.get() != GameState.BET) {
            throw new Error("Incorrect GameState to stop game: " + gameState.get());  
        }
        gameState.set(GameState.HANDLE_USER_ACTION);
        deal.clean();
        shoe.clean();
        gameState.set(GameState.SHUFFLE);
    }
    
    @Override
    public void userDealAction(DealAction act) {
        if (gameState.get() != GameState.DEAL) {
            throw new Error("Incorrect GameState: " + gameState.get() + " for a deal action: " + act);  
        }
        gameState.set(GameState.HANDLE_USER_ACTION);
        StringProperty lastBid = deal.lastBid();
        if (lastBid != null) {
            lastBid.set(null); // to invalidate value            
            lastBid.set(act.toString());
        } else {
            throw new Error("Current hand should not be null");
        }
        deal.handle(act);
        checkDeal();
    }
    
    @Override
    public String verifyDealAction(DealAction act) {
        if (gameState.get() != GameState.DEAL) {
            throw new Error("Incorrect GameState: " + gameState.get() + " for a deal action: " + act);  
        }       
        return deal.verifyDealAction(act);        
    }
    
    @Override
    public ObjectProperty<GameStatistics> getStatistics() {
        return statistics;
    }
    
    private void checkDeal() {
        if (deal.availableActions().isEmpty()) {            
            deal.end();
            updateStatistics(deal.result());
            if (shoe.played() > shoe.total() / 2) {
                gameState.set(GameState.SHUFFLE);
            } else {
                gameState.set(GameState.BET);
            }
        } else {
            gameState.set(GameState.DEAL);
        }
    }

    void updateStatistics(DealResult dr) {
        statistics.get().newDeal(dr);        
        statistics.fireValueChangedEvent();
    }

    
    public Game() {
    }
    
    public static final Game GAME = new Game();

    @Override
    public Shoe getShoe() {
        return shoe;
    }

    @Override
    public DealModel getDealModel() {
        return deal;
    }

    @Override
    public int getOptimalBet(int minBet, int maxBet) {
        int tc = (int)getShoe().getTrueCount();
        int bet = minBet;
        BetMethod mb = rules.getBetMethod();
        switch (mb) {
            case MIN: return minBet;
            case MAX: return tc >= 2 ? maxBet : minBet;
            case PRO: return Math.min(Math.max(tc * bet, minBet), maxBet);
            default: throw new Error("Unknown bet method");
        }
    }

    private static class StatProperty extends SimpleObjectProperty<GameStatistics> {
        @Override
        public void fireValueChangedEvent() {
            super.fireValueChangedEvent();
        }
    }
    
    public void setRecording(File frFile) {
        try {
            fr = frFile != null ? new FlightRecorder.ON(frFile) : new FlightRecorder.OFF();            
        } catch (Exception e) {
            throw new Error(e);
        }
    }
    
    public void setRecording(PrintStream ps) {
        fr = new FlightRecorder.ON(ps);
    }
    
    private FlightRecorder fr = new FlightRecorder.OFF();
    
    
    @Override
    public Task<GameStatistics> checkStrategy(int shuffles, int hands) {
        Game g = new Game();
        g.rules.replecate(rules);
        g.shoe.setShuffledCards(shoe.getShuffledCards());
        g.fr = fr;
        return new GameTask(g, shuffles, hands);
    }
    
    private static class GameTask extends Task<GameStatistics> {
        final Game g;
        final int shuffles;
        final int hands;
        private GameTask(Game g, int shuffles, int hands) {
            this.g = g;
            this.shuffles = shuffles;
            this.hands = hands;
        }

        @Override
        public GameStatistics call() {
            g.begin();
            int count = 0;
            GameStatistics stat = g.statistics.get();
            try {      
                while(count < shuffles) {
                    Game.playShuffle(g, hands);
                    count++;
                    if (count % 1000 == 1) {
                        if (super.isCancelled()) {
                            return stat;
                        }
                        updateProgress(count, shuffles);
                        updateMessage("ROI: " + stat.percent());
                    }

                }
            } catch (Throwable t) {                    
                updateProgress(count, count);
                t.printStackTrace();
            }
            updateMessage("ROI: " + stat.percent());
            g.end();
            return stat;
        }        
    }
    
    public static void playShuffle(Game g, int hands) {
        g.userShuffle();
        g.fr.rules(g.getRules());
        g.fr.shuffleBegin(g.shoe);        
        GameStatistics stat = g.getStatistics().get();
        while(g.getGameState().get() != GameState.SHUFFLE) {
            switch (g.getGameState().get()) {
                case DEAL: {
                    DealAction act = g.getDealModel().optimalAction().get();
                    g.userDealAction(act);
                    g.fr.dealAction(g.shoe.played(), g.shoe.getRunningCount(), stat.win(), act);
                    break;
                }
                case BET: {               
                    int bet = g.getOptimalBet(50, 500);
                    g.userBet(bet, hands);
                    g.fr.betAction(g.shoe.played(), g.shoe.getRunningCount(), stat.win(), bet, hands);
                    break;
                }
            }
        }    
        g.fr.shuffleEnd(g.shoe);
    }
}