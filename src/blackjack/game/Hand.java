/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */


package blackjack.game;

import blackjack.model.Card;
import blackjack.model.DealAction;
import blackjack.model.HandModel;
import blackjack.model.HandState;
import blackjack.model.Strategy;
import blackjack.model.Strategy.Dealer;
import blackjack.model.Strategy.Player;
import javafx.beans.InvalidationListener;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Represents a set of cards
 */
public class Hand implements HandModel {
    private boolean isFinal = false;
    private boolean isDouble = false;
    private boolean isSurrender = false;
    private boolean allowSurrender = false;
    private boolean fromAceSplit = false; // if this hand result of an Ace split?
    private boolean fromSplit = false;    // if this hand result of a split?
    private boolean insurance = false;    // dealer hand, when insurance is available
    private int insuranceWin = 0;
    private int number = 0; // number of hand in the rows (could be changed during the game if split happens)
    private InvalidationListener listener;
    
    private final IntegerProperty resultProperty;
    private final StringProperty lastBidProperty;
    private final ObservableList<Card> cards;
    
    public Hand() {
        cards = FXCollections.observableArrayList();
        resultProperty = new SimpleIntegerProperty(Integer.MAX_VALUE);
        lastBidProperty = new SimpleStringProperty();
    }

    public final Hand add(Card card) {
        cards.add(card);
        updatePoints();
        return this;
    }
    
    public final Card remove() {
        int last = cards.size() - 1;
        Card c = cards.get(last);
        cards.remove(last);
        updatePoints();
        return c;
    }
    
    @Override
    public boolean isAfterSplit() {
        return fromSplit;
    }
            
    private void updatePoints() {
        int points = calcPoints(true);
        if (points >= 21) {
            isFinal = true;
        } else if (fromAceSplit && !isSplitable() && cards.size() == 2) {
            isFinal = true;
        }
    }
    
    private int calcPoints(boolean skipClosed) {
        int p = 0;
        int a = 0;
        for (Card c: cards) {
            if (skipClosed && !c.isOpen()) {
                continue;
            }
            p += c.points;
            if (c.isAce) {
                a++;
            }
        }
        while (p > 21 && a > 0) {
            p = p - 10;
            a--;
        }
        return p;
    }
    
    /**
     * @return true if a hand can be split
     */
    boolean isSplitable() {
        if (cards.size() != 2) {
            return false;
        }
        Card c0 = cards.get(0);
        Card c1 = cards.get(1);
        
        return (c0.points == c1.points && c0.isOpen() && c1.isOpen());
    }
    
    boolean isDoubleable() {
        return !isDouble && !fromAceSplit && cards.size() == 2;
    }
    boolean isSurrenderable() {
        return allowSurrender && !fromSplit && cards.size() == 2;
    }
    
    void setAllowSurrender(boolean flag) {
        allowSurrender = flag;
    }
    
    /**
     * @param f true, if hand is from any split (bj is just 21)
     */
    void setHandObtainedFromSplit(boolean f) {
        fromSplit = f;
    }
    boolean isHandObtainedFromSplit() {
        return fromSplit;
    }
    
    void setAceSplit(boolean f) {
        fromAceSplit = f;
    }

    boolean isAceSplit() {
        return fromAceSplit;
    }
    
    void setInsurance(boolean flag) {        
        if (insurance != flag && cards.size() > 0) {
            insurance = flag;
            if (listener != null) {                
                listener.invalidated(this);
            }
        } else {
            insurance = flag;
        }
        
    }

    boolean isInsurance() {
        return insurance;
    }
        
    void setInsuranceWin(int amount) {
        insuranceWin = amount;
    }

    int getInsuranceWin() {
        return insuranceWin;
    }
        
    public boolean isBlackJack() {
        return (cards.size() == 2 && calcPoints(false) == 21 && !fromSplit);
    }
    
    @Override
    public int softPoints() {
        int points = points();
        if (isFinal() || points < 11) {
            return points;
        }
        int soft = 0;
        for (Card c: cards) {
            if (!c.isOpen()) {
                return points;
            } else if (c.isAce) {
                soft += 1;
            } else {
                soft += c.points;
            }
        }
        return soft;
    }
    
    
    @Override
    public int points() {
        return calcPoints(true);
    }

    public boolean isFinal() {
        return isFinal;
    }
    
    @Override
    public int number() {
        return number;
    }
    
    void setNumber(int n) {
        this.number = n;
    }
    void shift() {
        number++;
    }
    
    /**
     * When player said: enough
     */
    public void stop() {
        isFinal = true;
    }
    public void start() {
        isFinal = false;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Card c: cards) {
            sb.append(c.toString()).append(" ");            
        }
        sb.append("p:").append(points()).append("/").append(softPoints());
        if (resultProperty.get() != Integer.MAX_VALUE) {
            sb.append(" result: ").append(resultProperty.get());
        }
        return sb.toString();
    }

    @Override
    public ObservableList<Card> cards() {
        return cards;
    }

    @Override
    public HandState state() {
        if (insurance) {
            return HandState.INSURANCE;
        }
        if (isSurrender) {
            return HandState.SURRENDER;
        }
        if (isBlackJack()) {
            return HandState.BLACK_JACK;
        }
        int p = points();
        if (p < 21) {
            return HandState.LESS;
        } if (p > 21) {
            return HandState.BUST;
        } else {
            return HandState.JUST;
        }        
    }
    int calcHand(boolean isDealerBJ, int dp, int bet) {
        int result = isSurrender ? bet / -2 : _calcHand(isDealerBJ, dp, bet);
        result += insuranceWin;
        resultProperty.set(result);
        return result;
    }

    int _calcHand(boolean isDealerBJ, int dp, int bet) {
        int p = points();
        
        if (isDealerBJ) {
            return isBlackJack() ? 0 : - bet;
        }
        
        if (isBlackJack()) {
            return bet * 3 / 2;
        }
        
        if (isDouble) {
            bet = bet * 2;
        }
        
        if (p > 21) { // hand busted
            return - bet;
        }
        
        if (dp > 21) { // dealer busted
            return bet;
        }
        
        if (dp == p) {
            return 0;
        } else if (dp < p) {
            return bet;
        } else {
            return -bet;
        }
        
    }
    
    /*
     * User doubles
     */
    Hand setDouble() {
        isDouble = true;
        return this;
    }
    
    /*
     * Does user already double?
     */
    boolean isDouble() {
        return isDouble;
    }
    
    /*
     * User surrenders
     */
    Hand setSurrender() {
        isSurrender = true;
        isFinal = true;
        Card c = cards.remove(0);
        cards.add(0, c);
        return this;
    }
    /*
     * Does user already surrender?
     */
    boolean isSurrender() {
        return isSurrender;
    }

    @Override
    public IntegerProperty result() {
        return resultProperty;
    }

    @Override
    public StringProperty lastBid() {
        return lastBidProperty;
    }

    void openClosedCards() {
        Card c = cards.get(cards.size() - 1);
        if (!c.isOpen()) {
            c.setOpen(true);
            updatePoints();
            if (listener != null) {
                listener.invalidated(this);
            }
        }
    }

    Strategy.Player playerState() {
        if (isSplitable()) {
            switch (Card.RANKS[cards.get(0).rank]) {
                case 'A': return Player.PA;
                case '2': return Player.P2;
                case '3': return Player.P3;
                case '4': return Player.P4;
                case '5': return Player.P5;
                case '6': return Player.P6;
                case '7': return Player.P7;
                case '8': return Player.P8;
                case '9': return Player.P9;
                default: return Player.PT;
            }
        }
        int soft = softPoints();
        int hard = points();
        
        if (soft != hard) {
            switch (soft) {
                case 3: return Player.A2;
                case 4: return Player.A3;
                case 5: return Player.A4;
                case 6: return Player.A5;
                case 7: return Player.A6;
                case 8: return Player.A7;
                case 9: return Player.A8;
                case 10: return Player.A9;
                default: throw new Error("Illegal soft points: " + soft);
            }
        } 
        switch (hard) {
            case 5: return Player.H7;
            case 6: return Player.H7;
            case 7: return Player.H7;
            case 8: return Player.H8;
            case 9: return Player.H9;
            case 10: return Player.H10;
            case 11: return Player.H11;
            case 12: return Player.H12;
            case 13: return Player.H13;
            case 14: return Player.H14;
            case 15: return Player.H15;
            case 16: return Player.H16;
            case 17: return Player.H17;
            default: return Player.H18;
        }        
    }

    Strategy.Dealer dealerState() {
        switch (points()) {
            case 2: return Dealer.D2;
            case 3: return Dealer.D3;
            case 4: return Dealer.D4;
            case 5: return Dealer.D5;
            case 6: return Dealer.D6;
            case 7: return Dealer.D7;
            case 8: return Dealer.D8;
            case 9: return Dealer.D9;
            case 10: return Dealer.DT;
            case 11: return Dealer.DA;
        }
        throw new Error("Incorrect dealer points " + points());
    }
    
    DealAction action(Strategy.Act a) {
        switch (a) {
            case Hit: return DealAction.HIT;
            case Stand: return DealAction.STAND;
            case Split: return DealAction.SPLIT;
            case Double: return isDoubleable() ? DealAction.DOUBLE : DealAction.HIT;
            case DoubleStand: return isDoubleable() ? DealAction.DOUBLE : DealAction.STAND;
            case Surrender: return isSurrenderable() ? DealAction.SURRENDER : DealAction.HIT;
            case SurrenderStand: return isSurrenderable() ? DealAction.SURRENDER : DealAction.STAND;
        }
        throw new Error("Unknown act " + a);
    }

    /**
     * For the sake of simplicity only one listener is allowed.
     * @param listener 
     */
    @Override
    public void addListener(InvalidationListener listener) {
        this.listener = listener;
    }

    @Override
    public void removeListener(InvalidationListener listener) {
        this.listener = null;
    }
}
