/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */
package blackjack.model;

import java.util.List;

/**
 * Interface for storage set of rules.
 * Each rule is associated with a Prop - class that store:
 * <ul>
 * <li> current value
 * <li> list of possible String values
 * </ul>
 * 
 * @author Dmitry Fazunenko
 */
public interface GameRules {
    enum Group {
        RULE, // to be displayed in the rule list        
        EDITABLE, // to be suggested for modification in the Rules menu
        STRATEGY_MENU, // to be suggested for modification in the Strategy menu
        AFFECT_STRATEGY, // change causes new strategy
        SURRENDER, // used by GUIContorller to show/hide surrender button
        MILLION // to be displayed in PlayMillion dialog
    };
    
    /**
     * Return list of rules which belongs to the group
     * @param group
     * @return list
     */
    public List<Prop> list(Group... group);

    /**
     * Sets the rules according to give string.
     * Note: this method is for the sake of testability.
     * @param str in format which could be parsed.
     */
    public void fromString(String str);

    /**
     * @return string representation of all rules.
     */
    public String asString();
    
}
