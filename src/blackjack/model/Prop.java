/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.model;

import blackjack.model.GameRules.Group;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Class to represent name-value pair with a list of possible values.
 * 
 * @author Dmitry Fazunenko
 */
public class Prop {
    
    
    /**
     * Name of the property
     */
    public final String name;
    
    /**
     * List of possible values.
     */
    public final ObservableList<String> menuItems;
    
    /**
     * The current value.
     */    
    public final StringProperty value;
    
    private final List<Group> groups = new ArrayList<>();
    
    public Prop(String name, List<String> items, String value, Group... groups) {
        this.name = name;
        this.menuItems = items == null ? null : FXCollections.observableList(items);
        this.value = new SimpleStringProperty(value);
        this.groups.addAll(Arrays.asList(groups));        
    }
    
    public boolean belongs(Group... selected) {
        return groups.containsAll(Arrays.asList(selected));
    }
    
    public int index() {        
        for (int i = 0; i < menuItems.size(); i++) {
            if (value.get().equals(menuItems.get(i))) {
                return i;
            }
        }
        return -1;
    }
}
