/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.model;

import java.util.List;

/**
 * Interface for strategy.
 * Strategy tells which action is the most optimal in current situation.
 * A strategy may either use or not use "indices". If indices are not used
 * the strategy is called "basic". If indices are used the optimal action
 * depends on the current "true-count".
 * 
 * @author Dmitry Fazunenko
 */
public interface Strategy {
    enum Player {
        H7, H8, H9, H10, H11, H12, H13, H14, H15, H16, H17, H18, // hard
        A2, A3, A4, A5, A6, A7, A8, A9, // soft
        P2, P3, P4, P5, P6, P7, P8, P9, PT, PA;  // split
        
        @Override
        public String toString() {
            char c = name().charAt(0);
            String points = name().substring(1);
            switch(c) {
                case 'H': {
                    switch (points) {
                        case "7": return "Hard 7-";
                        case "18" : return "Hard 18+";
                        default: return "Hard " + points;
                    }
                }
                case 'A': return "Ace + " + points;
                case 'P': return "(" + points + "," + points + ")";
            }
            throw new Error("should not reach here");
        }
    };
        
    enum Dealer {
        D2, D3, D4, D5, D6, D7, D8, D9, DT, DA;
        
        @Override
        public String toString() {
            return name().substring(1);
        }
        
    }

    enum Act {
        Hit, Stand, Split, Double, DoubleStand, Surrender, SurrenderStand
    }
    
    enum IndexType {
        HARD("Hard Hit/Stay"),
        HARD_DOUBLE("Hard Double"),
        SOFT_DOUBLE("Soft Double"),
        SPLITS("Splits"),
        SURRENDER("Surrdender");

        public final String kind;
        IndexType(String kind) {
            this.kind = kind;
        }
    };
    
    /**
     * @param dealer - dealer open card
     * @param hand - player hand
     * @return optimal actions for the current dealer,player pair
     */    
    public Acts actions(Dealer dealer, Player hand);
    
    /**
     * @return List of indices for the current strategy.
     */
    public List<StrategyIndex> getIndices();

    /**
     * @return List of indices for the current strategy.
     */
    public boolean useIndices();
    
    /**
     * @return strategy without indices. 
     */
    public Strategy getBasicStrategy();
            
    /**
     * Incapsulate two possible actions with the separator - a true count value. 
     */
    public static final class Acts {
        public final Act passive, agressive;
        public final int trueCount;

        public Acts(Act always) {
            this(always, always, -111);            
        }
        
        public Acts(Act lessThan, Act greaterThan, int trueCount) {
            this.passive = lessThan;
            this.agressive = greaterThan;
            this.trueCount = trueCount;
        }
        
        public Act choose(float tc) {
            return tc < trueCount ? passive : agressive;                    
        }
        
        public boolean isSimple() {
            return passive == agressive;
        }
        
        @Override
        public String toString() {
            if (isSimple()) {
                return passive.toString();
            } else {
                return passive.toString() + "(<" + trueCount + ");   "
                        +  agressive.toString() + "(>=" + trueCount + ")";
            }
        }
    }
    
    public interface StrategyIndex {
        IndexType type();
        Player player();
        Dealer dealer();
        int trueCount();
    }
    
}
