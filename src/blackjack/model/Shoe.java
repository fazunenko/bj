/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

/**
 * Class to represent several decks.
 * 
 * @author Dmitry Fazunenko
 */
public class Shoe implements Observable {
    
    private final List<InvalidationListener> observers = new ArrayList<>();
        
    // cards in a single deck
    public static final int LEN = 52;
        
    private int cards[];
    private int current;
    private int shuffled[] = null;
    
    // to calculate true count we need to now which dealt cards are not visible
    private List<Integer> closedCards = new ArrayList<>();

    // to calculate the pair-index
    private final List<Integer> playedCards = new ArrayList<>();
    
    public Shoe() {
    }
    
    public void shuffle(int deckCount) {
        current = 0;
        int total = LEN * deckCount;
        this.cards = new int[total];
        if (shuffled == null) {
            List<Integer> unsorted = new ArrayList<>(total);
            for (int i = 0; i < total; i++) {
                unsorted.add(i % LEN);
            }
            Random rnd = new Random();
            for (int i = 0; i < total; i++) {
                cards[i] = unsorted.remove(rnd.nextInt(unsorted.size()));
            }
        } else {
            for (int i = 0; i < total; i++) {
                cards[i] = shuffled[i % shuffled.length];
            }
        }
        playedCards.clear();
        update();
    }
    
    public void clean() {
        current = total();
        endDeal();
        update();
        
    }
    
    /**
     * Informs shoe that deal is over.
     * (It means all closed cards are now open).
     */
    public void endDeal(){
        closedCards.clear();
        update();
    }
    
    public int[] snapshot() {
        int[] snapshot = new int[cards.length];
        System.arraycopy(cards, 0, snapshot, 0, cards.length);
        return snapshot;
    }
                
    public Card nextClosedCard() {
        closedCards.add(current);
        Card c = nextCard();
        c.setOpen(false);
        return c;
    }
    
    public Card nextCard() {
        if (current >= cards.length) {
            throw new Error("Empty shoe");
        }
        current++;
        update();
        return new Card(cards[current-1]);
    }
    
    public int played() {
        return current;
    }
    
    public int total() {
        return cards.length;
    }
    
    /**
     * +1 for  2,3,4,5,6
     * 0 for 7,8,9
     * -1 for 10,11
     * @return running count for the current shoe.
     */
    public int getRunningCount() {
        int c = 0;
        for (int i = 0; i < current; i++) {
            if (closedCards.contains(i)) {
                continue;
            }
            int points = new Card(cards[i]).points;
            if (points < 7) {
                c++;
            } else if (points > 9) {
                c--;
            }
        }
        return c;
    }
    /**
     * True count = Running count / Decks Remaining.
     * @return true count rounded to one decimal place.
     */
    public float getTrueCount() {
        if (total() == played()) {
            return 0;
        }        
        return  Math.round(getRunningCount() * 5200f / (total() - played() + closedCards.size())) / 100f ;
    }
    
    private void update() {
        observers.forEach((observer) -> {
            observer.invalidated(this);
        });
    }

    @Override
    public void addListener(InvalidationListener listener) {
        observers.add(listener);
    }

    @Override
    public void removeListener(InvalidationListener listener) {
        observers.remove(listener);
    }

    /**
     * For the sake of testing only
     * @param shuffled array of card id.
     */
    public void setShuffledCards(int[] shuffled) {
        this.shuffled = shuffled;
    }

    public int[] getShuffledCards() {
        return shuffled;
    }
    
    public void setShuffledCards(String shuffled) {
        String[] ccc = shuffled.split(" ");
        int[] ids = new int[ccc.length];
        for (int i = 0; i < ccc.length; i++) {
            ids[i] = Card.rs2id(ccc[i]);
        }
        setShuffledCards(ids);
    }        
    
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = current; i < cards.length; i++) {
            sb.append(new Card(cards[i]).toString()).append(" ");
        }
        return sb.toString().trim();
    }
}
