/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.model;

import javafx.beans.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

/**
 * Interface to a Black Jack hand regardless player or dealer.
 * 
 * @author Dmitry Fazunenko
 */
public interface HandModel extends Observable {
    /**
     * @return list of hand cards.
     */
    public ObservableList<Card> cards();

    /**
     * @return the property storing information about the last bid for that hand.
     */
    public StringProperty lastBid();
    
    /**
     * @return the hard points for the hand.
     */    
    public int points();
    
    /**
     * @return the soft points for the hand.
     */    
    public int softPoints();
    
    /**
     * @return current hand state.
     */    
    public HandState state();
    
    /**
     * @return the property storing the result of the hand. 
     *         Result is not available until game is played.
     */    
    public IntegerProperty result();
    
    /**
     * @return number of the current hand in row of hands. (for dealer - always 0)
     */
    public int number();
    
    /**
     * @return true, if the hand is result of a split.
     */
    public boolean isAfterSplit();
}
