/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.model;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import javafx.util.StringConverter;

/**
 * Class responsible for collecting statistics.
 * It's supposed to be notified on the end of deal and shuffle.
 * 
 * @author Dmitry Fazunenko
 */
public class GameStatistics extends StringConverter<GameStatistics> {
    private int deals = 0;
    private int shuffles = 0; 
    private long win = 0;
    private long bet = 0;
        
    public GameStatistics() {        
    }
    
    public void newDeal(DealResult dr) {
        deals++;
        win += dr.totalWin;
        bet += dr.totalBet;
    }
    
    public void newShuffle() {
        shuffles++;
    }
    
    public int deals() {
        return deals;
    }
    
    public int shuffles() {
        return shuffles;
    }
    
    public long win() {
        return win;
    }
    
    public long bet() {
        return bet;
    }
    
    private static final NumberFormat NF = new DecimalFormat("##0.0");
    
    @Override
    public String toString() {
        return "win: " + win + "; " + " bet: " + bet + "; shuffles: " + 
                shuffles + "; deals" + deals;
    }
    
    private static final NumberFormat PF = new DecimalFormat("#,###,##0.00%");    
    public String percent() {
        float percent = bet != 0 ? ((float)win)/bet : 0 ;
        return PF.format(percent);
    }
    
    @Override
    public String toString(GameStatistics object) {
        return object.percent();
    }

    @Override
    public GameStatistics fromString(String string) {
        throw new Error("Not supported by design"); 
    }
    
}
