/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */
package blackjack.model;

/**
 * Possible user action during the deal.
 * 
 * @author Dmitry Fazunenko
 */
public enum DealAction {
    SPLIT("Split"),
    HIT("Hit"),
    STAND("Stand"),
    DOUBLE("Double"), 
    SURRENDER("Surrender"),
    INSURANCE_YES("Insurance: yes"),
    INSURANCE_NO("Insurance: no");

    private final String visible;
    DealAction(String visible) {
        this.visible = visible;
    }
    
    @Override
    public String toString() {
        return visible;
    }
}
