/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.model;

/**
 * List of possible hand states.
 * 
 * @author Dmitry Fazunenko
 */
public enum HandState {
    BLACK_JACK, LESS, JUST, BUST, INSURANCE, SURRENDER
}
