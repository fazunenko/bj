/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 *
 */

package blackjack.model;

/**
 * List of possible game states.
 * @author Dmitry Fazunenko
 */
public enum GameState {
    SHUFFLE, BET, DEAL, HANDLE_USER_ACTION
    
}
