/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.model;

/**
 * Represent the entire deal result as total amount of bets and total win.
 * 
 * @author Dmitry Fazunenko.
 */
public class DealResult {
    /**
     * Total win of the player in the deal on all hands
     */
    public final int totalWin;
    
    /**
     * Total bet of the player in the deal on all hands
     */
    public final int totalBet;
    
    public DealResult(int totalWin, int totalBet) {
        this.totalWin = totalWin;
        this.totalBet = totalBet;
    }
}
