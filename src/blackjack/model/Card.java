/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */
package blackjack.model;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

/**
 * Card representation.
 * 
 * @author Dmitry Fazunenko
 */
public class Card implements Observable {
    
    public static final char[] RANKS = {'A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K'};
    public static final char[] SUITS = {'s', 'c', 'd', 'h'};
    public final int id;
    public final int suite;
    public final int rank;
    public final int points;
    public final boolean isAce;
    private boolean isOpen = true;
    InvalidationListener listener = null;

    public Card(int id) {
        this(id, true);
    }

    public Card(int id, boolean isOpen) {
        this.isOpen = isOpen;
        if (id < 0 || id >= Shoe.LEN) {
            throw new Error("Illegal card " + id);
        }
        this.id = id;
        this.suite = id / 13;
        this.rank = id % 13;
        this.isAce = (rank == 0);
        if (isAce) {
            points = 11;
        } else if (rank >= 9) {
            points = 10;
        } else {
            points = rank + 1;
        }
    }

    public Card setOpen(boolean b) {
        isOpen = b;
        if (listener != null) {
            listener.invalidated(this);
        }
        return this;
    }

    public boolean isOpen() {
        return isOpen;
    }

    @Override
    public String toString() {
        return isOpen ? RANKS[rank] + "" + SUITS[suite] : "XX";
    }
   
    public int visibleID() {
        return isOpen ? id : -1;
    }
    
    /**
     * rs - means rank/suite
     * "As" --> 0, "2h" -> 14 , etc
     * @param rs string representation of a card
     * @return card id
     */
    public static int rs2id(String rs) {
        char r = rs.charAt(0);
        char s = rs.charAt(1);
        int rank = -1;
        for (int i = 0; i < RANKS.length; i++) {
            if (r == RANKS[i]) {
                rank = i;
                break;
            }
        }
        int suite = -1;
        for (int i = 0; i < SUITS.length; i++) {
            if (s == SUITS[i]) {
                suite = i;
                break;
            }
        }
        return 13 * suite + rank;
    }    

    /**
     * For the sake of simplicity only one listener is supported.
     * The listener will be notified when setOpen() method is invoked on the card.
     * @param listener 
     */
    @Override    
    public void addListener(InvalidationListener listener) {
        this.listener = listener;
    }

    @Override
    public void removeListener(InvalidationListener listener) {
        this.listener = null;
    }
}
