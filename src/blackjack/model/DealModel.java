/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */
package blackjack.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.collections.ObservableList;

/**
 * Interface to access the current deal.
 * 
 * @author Dmitry Fazunenko
 */
public interface DealModel {
    /**
     * Returns the object representing the dealer hand.
     * It supposed the instance will not change from deal to deal.
     * @return hand of the dealer
     */
    public HandModel dealerHand();
    
    /**
     * Returns the object representing the player hands
     * 
     * It supposed the instance will not change from deal to deal.
     * @return list of player hands
     */
    public ObservableList<? extends HandModel> playerHands();
    
    /**
     * Returns the property object containing the number of the current player 
     * hand.
     * 
     * It supposed the instance will not change from deal to deal.
     * 
     * @return list of player hands
     */    
    public IntegerProperty currentHandNum();
    
    /**
     * @return return the list of actions available at the moment. 
     */
    public ObservableList<DealAction> availableActions();
    
    /**
     * @return the property containing the action which is most optimal
     * at the moment.
     */
    public ObjectProperty<DealAction> optimalAction();
    
    /**
     * Returns the amount of dealer points in terms of Strategy, like Dealer.DT.
     * This is needed to properly display the optimal action
     * in the StrategyView.
     * 
     * @return the dealer points in terms of Strategy
     * @see #playerState() 
     */
    public Strategy.Dealer dealerState();

    /**
     * For the current player hand returns the value in terms of Strategy.
     * Note: returning just points is not enough, 6 points could be either
     * Hard6, Soft5 or Split-of-3.
     * 
     * @return the current points in terms of Strategy
     * @see #dealerState()
     */
    public Strategy.Player playerState();
    
    /**
     * Returns deal result.
     * If deal is not completed, returns null. DealResulp object is supposed 
     * to be created each time the deal is completed.
     * 
     * @return deal result or null if deal is still playing
     */
    public DealResult result();
}
