/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.model;

import javafx.beans.property.ObjectProperty;
import javafx.concurrent.Task;

/**
 * Representation of the entire game - a sequence of shuffles.
 * 
 * @author Dmitry Fazunenko
 */
public interface GameModel {    
    /**
     * Method to be called once when a new game is about to start.
     */
    public void begin();

    /**
     * Method to be called once when the game is finished.
     */
    public void end();
    
    /**
     * Informs model that the user turned autoplay on/off
     * @param on true for on, false for off
     */
    public void userAutoPlay(boolean on);    

    /**
     * Informs model that the user started a new shuffle.
     */
    public void userShuffle();

    /**
     * Informs model that the user started a new deal.
     * @param bet - bet amount
     * @param hands - number of boxes
     */
    public void userBet(int bet, int hands);

    /**
     * Informs model that the user stopped the current deal.
     */
    public void userEndShuffle();

    /**
     * Informs model that the user did some deal action.
     * @param act - the action performed by user
     */
    public void userDealAction(DealAction act);
    
    public boolean isAutoPlay();
    public Shoe getShoe();
    public DealModel getDealModel();
    public ObjectProperty<GameState> getGameState();
    public ObjectProperty<GameStatistics> getStatistics();
    public int getOptimalBet(int minBet, int maxBet);
    public GameRules getRules();
    public ObjectProperty<Strategy> getStrategy();
    
    /**
     * Verifies if the action by player is optimal or not.
     * @param d action to verify
     * @return null if action is optimal or error message with comments
     */
    public String verifyDealAction(DealAction d);
    
    /**
     * Create Task to be executed not in the FX thread to verify efficiency 
     * of the selected strategy options.
     * @param shuffles number of shuffles to play
     * @param hands number of player boxes in a deal
     * @return Task
     */
    public Task<GameStatistics> checkStrategy(int shuffles, int hands);
}
