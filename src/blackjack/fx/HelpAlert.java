/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;

import javafx.scene.control.Alert;
import javafx.scene.web.WebView;

/**
 *
 * @author Fa
 */
class HelpAlert extends BJDialog {    
    HelpAlert() {
        super(Alert.AlertType.INFORMATION);
        super.setHeaderText("BJ Simulator brief description");
        WebView helpView = new WebView();    
        helpView.getEngine().load(getClass().getResource("help.html").toExternalForm());
        getDialogPane().setContent(helpView);
   }    
}
