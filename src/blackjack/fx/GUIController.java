/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;

import blackjack.model.DealAction;
import static blackjack.model.DealAction.HIT;
import blackjack.model.DealModel;
import blackjack.model.GameState;
import blackjack.model.GameModel;
import blackjack.model.GameRules;
import blackjack.model.GameRules.Group;
import blackjack.model.GameStatistics;
import blackjack.model.Prop;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Control;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.SpinnerValueFactory.IntegerSpinnerValueFactory;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Background;
import javafx.scene.layout.VBox;

/**
 * FX Controller.
 * Class that connects model and view. (The main GUI class)
 * 
 * @author Dmitry Fazunenko
 */
public class GUIController implements Initializable {
                    
    @FXML private Node mainPane;
    @FXML private Menu strategyMenu, rulesMenu;
    @FXML private MultiHandView playerView;           
    @FXML private DealerHandView dealerView;
    @FXML private StrategyView strategyView;
    @FXML private HintView hintView;
    @FXML private Text totalText;
    @FXML private Spinner<Integer> betSpinner, handSpinner;
    @FXML private Button standButton, hitButton, doubleButton, splitButton, surrenderButton;
    @FXML private Button insuranceYesButton, insuranceNoButton;
    @FXML private Button dealButton;
    @FXML private Pane dealButtonPane, insuranceButtonPane, shuffleButtonPane;
    @FXML private MenuItem newShuffleMenuItem, newTrainingMenuItem;
    @FXML private CheckMenuItem trueCountMenuItem, optimalActionMenuItem, 
            displayStrategyMenuItem, autoPlayMenuItem, 
            trainModeMenuItem, showHandPointsMenuItem, showShoeSizeMenuItem,
            visualEffectMenuItem;
    
    private final BJDialog bjAlert = createWarningAlert(AlertType.WARNING);
    private final BJDialog bjDialog = createWarningAlert(AlertType.INFORMATION);
    private final BJDialog strategyDialog = createWarningAlert(AlertType.INFORMATION);
    private final BJDialog helpDialog = new HelpAlert();    
    private final MillionPlayAlert millionPlayDialog = new MillionPlayAlert();
    private final List<BJDialog> dialogs = Arrays.asList(bjDialog, bjAlert,
            helpDialog, millionPlayDialog, strategyDialog);
    private Prop surrenderProp;
        
    @FXML private void handleVisualEffects() {
        
    }

    @FXML private void handleNewShuffle() {
        model.userEndShuffle();
    }

    @FXML private void handleNewTraining() {
        model.end();
        model.begin();
    }

    @FXML private void handleHelp() {
        helpDialog.displayAndWait();
    }
        
    @FXML private void handleExit() {
        model.end();
        System.exit(0);
    }
    
    @FXML private void handlePlayMillion() {
        int shuffles = 3_000_000;
        int hands = handSpinner.getValue();
        Task<GameStatistics> task = model.checkStrategy(shuffles, hands);
        new Thread(task).start();        
        List<String> props = new ArrayList<>();
        for (Prop p: model.getRules().list(Group.MILLION)) {
            props.add(p.value.get());
        }
        props.add("#hands " + hands);
        
        millionPlayDialog.display(task, props);
        if (task.isRunning()) {
            task.cancel();
        }
    }
    
    /**
     * @return Alert dialog in the necessary style.
     */
    private BJDialog createWarningAlert(AlertType type) {        
        BJDialog alert = new BJDialog(type);
        alert.setTitle("");
        //alert.getDialogPane().getStylesheets().add(getClass().getResource("normalSize.css").toExternalForm());
        return alert;
    }
       
    @FXML private void doDeal() {
        dealerView.fadeDown();
        playerView.fadeDown();
        model.userBet(betSpinner.getValue(), handSpinner.getValue());
        dealerView.fadeUp();
        //playerView.fadeUp();
    }
    @FXML private void doStand() {        
        doDealAction(DealAction.STAND);
    }
    @FXML private void doHit() {
        doDealAction(DealAction.HIT);
    }
    @FXML private void doDouble() {
        doDealAction(DealAction.DOUBLE);
    }
    @FXML private void doSplit() {
        doDealAction(DealAction.SPLIT);
    }
    @FXML private void doInsuranceYes() {
        doDealAction(DealAction.INSURANCE_YES);
    }
    @FXML private void doInsuranceNo() {
        doDealAction(DealAction.INSURANCE_NO);
    }
    @FXML private void doSurrender() {
        doDealAction(DealAction.SURRENDER);
    }
    
    private void doDealAction(DealAction d) {
        if (trainModeMenuItem.isSelected() && !model.isAutoPlay()) {
            String errorMessage = model.verifyDealAction(d);
            if (errorMessage != null) {
                bjAlert.setHeaderText("'" + d + "' is not optimal now. '" + model.getDealModel().optimalAction().get() + "' is better.");
                bjAlert.setContentText(errorMessage);
                bjAlert.displayAndWait();                
            }
        }
        model.userDealAction(d);
    }

    @FXML private void displayCurrentStrategy() {
        strategyDialog.setHeaderText("Strategy: " + strategyView.strategy);
        strategyDialog.getDialogPane().setContent(strategyView.getLargeStrategyView());
        strategyDialog.displayAndWait();
    }
    @FXML private void displayIndices() {
        strategyDialog.setHeaderText("Indices: "  + strategyView.strategy);
        strategyDialog.getDialogPane().setContent(strategyView.getLargeIndicesView());
        strategyDialog.displayAndWait();
    }

    @FXML private void displayRules() {
        bjDialog.setHeaderText("Rules:");
        VBox pane = new VBox();
        ObservableList<Node> kids = pane.getChildren();
        for (Prop p: model.getRules().list(GameRules.Group.RULE)) {
            Text txt = new Text(p.value.get());
            txt.getStyleClass().add("textDialog");
            kids.add(txt);
        }
        bjDialog.getDialogPane().setContent(pane);
        bjDialog.displayAndWait();
    }
    @FXML private void displayStatistics() {
        bjDialog.setHeaderText("Statistics");
        VBox pane = new VBox();
        ObservableList<Node> kids = pane.getChildren();
        GameStatistics s = model.getStatistics().get();
        
        kids.add(new Text("Shuffles: " + s.shuffles()));
        kids.add(new Text("Deals: " + s.deals()));
        kids.add(new Text("Bet: $" + s.bet()));
        kids.add(new Text("Win: $" + s.win()));
        kids.add(new Text(""));
        kids.add(new Text("ROI: " + s.percent()));
        

        for (Node n: kids) {
            n.getStyleClass().add("textDialog");
        }
        bjDialog.getDialogPane().setContent(pane);
        bjDialog.displayAndWait();
    }
        
    @FXML private void doShuffle() {        
        model.userShuffle();
    }
    
    @FXML private void doTogleAutoplay() {
        if (model.isAutoPlay()) {
            model.userAutoPlay(false);
            hintView.toggleAutoplayButton.setText("Start autoplay");
        } else {
            model.userAutoPlay(true);
            hintView.toggleAutoplayButton.setText("Stop autoplay");
        }
        
    }

    private GameModel model;
    private Runnable autoBid;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Main.controller = this;
    }
       
    public void setModel(GameModel gmodel) {        
        model = gmodel;
        model.getGameState().addListener(new GameStateListener());
        model.getStatistics().addListener(new StatisticsListener());
        autoBid = new AutoBid(model);
                
        DealModel deal = model.getDealModel();
        deal.optimalAction().addListener(new OptimalActionListener());
        dealerView.setHand(deal.dealerHand());       
        playerView.setHands(deal.playerHands(), deal.currentHandNum());
        
        //model.getShoe().addListener(new ShoeListener());
               
        displayStrategyMenuItem.selectedProperty().addListener(new CheckMenuItemListener(strategyView));
        strategyView.setVisible(displayStrategyMenuItem.isSelected());
        boolean needSeparator = false;
        for (Prop p: model.getRules().list(GameRules.Group.STRATEGY_MENU)) {
            if (needSeparator) {
                strategyMenu.getItems().add(new SeparatorMenuItem());
            } else {
                needSeparator = true;
            }
            addPropEdit(strategyMenu, p);
        }
        strategyView.setStrategy(model.getStrategy());
        strategyView.visibleProperty().addListener(new CardResizeListener());
        
        needSeparator = false;
        for (Prop p: model.getRules().list(GameRules.Group.EDITABLE)) {
            if (needSeparator) {
                rulesMenu.getItems().add(new SeparatorMenuItem());
            } else {
                needSeparator = true;
            }
            addPropEdit(rulesMenu, p);            
        }
        surrenderProp = model.getRules().list(GameRules.Group.SURRENDER).get(0);
             
        hintView.setShoe(model.getShoe());
        hintView.toggleAutoplayButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                doTogleAutoplay();
            }
        });
        
        hintView.trueCountText.setVisible(trueCountMenuItem.isSelected());
        hintView.runningCountText.setVisible(trueCountMenuItem.isSelected());
        trueCountMenuItem.selectedProperty().addListener(new CheckMenuItemListener(hintView.trueCountText, hintView.runningCountText));
        
        hintView.optimalActionText.setVisible(optimalActionMenuItem.isSelected());        
        optimalActionMenuItem.selectedProperty().addListener(new CheckMenuItemListener(hintView.optimalActionText));
        optimalActionMenuItem.selectedProperty().addListener(new ShowOptimalListener());
        strategyView.setShowOptimal(optimalActionMenuItem.isSelected());
        CardView.setScale(strategyView.isVisible() ? 1.2 : 1.6);
        
        autoPlayMenuItem.selectedProperty().addListener(new CheckMenuItemListener(hintView.toggleAutoplayButton, hintView.autoplaySlider, hintView.sliderText));
        hintView.sliderText.setVisible(autoPlayMenuItem.isSelected());
        hintView.toggleAutoplayButton.setVisible(autoPlayMenuItem.isSelected());
        hintView.autoplaySlider.setVisible(autoPlayMenuItem.isSelected());
        
        showShoeSizeMenuItem.selectedProperty().addListener(new CheckMenuItemListener(hintView.shoeText));
        hintView.shoeText.setVisible(showShoeSizeMenuItem.isSelected());
               
        showHandPointsMenuItem.selectedProperty().addListener(new BoolPropToggle(HandPane.SHOW_POINTS));
        HandPane.SHOW_POINTS.set(showHandPointsMenuItem.isSelected());
        visualEffectMenuItem.selectedProperty().addListener(new BoolPropToggle(EffectManager.USE_EFFECTS));
        EffectManager.USE_EFFECTS.set(visualEffectMenuItem.isSelected());
        
        Arrays.asList(standButton, hitButton, doubleButton, splitButton, dealButton, surrenderButton).forEach((b) -> {
            b.setMinWidth(Control.USE_PREF_SIZE);
        });
        
        updateScreen();
    }    
    
    private void updateScreen() {
        String cssURL = getClass().getResource(Screen.current.css).toExternalForm();
        helpDialog.getDialogPane().setPrefSize(Screen.current.width * 0.9, Screen.current.height * 0.9);
        strategyDialog.getDialogPane().setPrefSize(Screen.current.width * 0.96, Screen.current.height * 0.6);
        strategyDialog.setResizable(true);        
        dialogs.forEach((alert) -> {
            Screen.updateCSS(alert.getDialogPane().getScene(), cssURL);
            alert.setMainPane(mainPane);
            alert.initOwner(Main.primaryStage);
        });
        Screen.updateCSS(Main.primaryStage.getScene(), cssURL);
    }
    
    private class StatisticsListener implements InvalidationListener {                
        @Override
        public final void invalidated(Observable observable) {
            GameStatistics s = ((ObjectProperty<GameStatistics>)observable).get();
            totalText.setText("Total: " + s.win() + "$");
        }        
    }
    
    /**
     * Reaction on changes to the Game State
     */
    private class GameStateListener implements InvalidationListener {
        @Override
        public void invalidated(Observable observable) {
            GameState newState = ((ObjectProperty<GameState>)observable).get();
            Runnable r = new Runnable() {
                @Override
                public void run() {                    
                    boolean auto = model.isAutoPlay();
                    newShuffleMenuItem.setDisable(auto || newState != GameState.BET);
                    newTrainingMenuItem.setDisable(auto);

                    betSpinner.setDisable(auto || newState != GameState.BET);
                    handSpinner.setDisable(auto || newState != GameState.BET);
                    dealButton.setDisable(auto || newState != GameState.BET);
                    hitButton.setDisable (auto || newState != GameState.DEAL);
                    ObservableList<DealAction> actions = model.getDealModel().availableActions();
                    hitButton.setDisable   (auto || newState != GameState.DEAL || !actions.contains(DealAction.HIT));
                    splitButton.setDisable (auto || newState != GameState.DEAL || !actions.contains(DealAction.SPLIT));
                    standButton.setDisable (auto || newState != GameState.DEAL || !actions.contains(DealAction.STAND));
                    doubleButton.setDisable(auto || newState != GameState.DEAL || !actions.contains(DealAction.DOUBLE));
                    surrenderButton.setDisable(auto || newState != GameState.DEAL || !actions.contains(DealAction.SURRENDER));
                    surrenderButton.setVisible(!surrenderProp.value.get().contains("No"));
                    insuranceYesButton.setDisable(auto || newState != GameState.DEAL || !actions.contains(DealAction.INSURANCE_YES));
                    insuranceNoButton.setDisable(auto || newState != GameState.DEAL || !actions.contains(DealAction.INSURANCE_NO));

                    rulesMenu.setDisable(auto || newState != GameState.SHUFFLE);

                    if (newState == GameState.SHUFFLE) {
                        shuffleButtonPane.setVisible(true);
                        dealButtonPane.setVisible(false);
                        insuranceButtonPane.setVisible(false);

                    } else if (actions.contains(DealAction.INSURANCE_YES)) {
                        shuffleButtonPane.setVisible(false);
                        dealButtonPane.setVisible(false);                        
                        insuranceButtonPane.setVisible(newState != GameState.HANDLE_USER_ACTION);
                    } else {
                        shuffleButtonPane.setVisible(false);
                        dealButtonPane.setVisible(!auto && newState != GameState.SHUFFLE);
                        insuranceButtonPane.setVisible(false);
                    }

                    if (!auto && newState == GameState.BET) {
                        dealButton.requestFocus();
                    } else if (!auto && newState == GameState.DEAL) {
                        if (optimalActionMenuItem.isSelected()) {
                            switch(model.getDealModel().optimalAction().get()) {
                                case HIT: hitButton.requestFocus(); break;
                                case SPLIT: splitButton.requestFocus(); break;
                                case STAND: standButton.requestFocus(); break;
                                case DOUBLE: doubleButton.requestFocus(); break;
                            }
                        }
                    } else if (auto && newState != GameState.HANDLE_USER_ACTION) {
                        doAutoPlay();
                    }
                }                
            };
            EffectManager.runAfter(r);
        }        
    }

    private void doAutoPlay() {
        new Thread(autoBid).start();
    }
    private void addPropEdit(Menu m, Prop p) {        
        ToggleGroup group = new ToggleGroup();                
        group.selectedToggleProperty().addListener(new PropEditListener(p));
        String selected = p.value.get();
        for (String val: p.menuItems) {
            RadioMenuItem rmi = new RadioMenuItem(val);
            rmi.setToggleGroup(group);
            m.getItems().add(rmi);
            if (selected.equals(val)) {
                group.selectToggle(rmi);
            }
        }
    }
            
    private class OptimalActionListener implements InvalidationListener {
        @Override
        public void invalidated(Observable observable) {
            DealAction optimalAction = ((ObjectProperty<DealAction>)observable).get();
            hintView.optimalActionText.setText(optimalAction == null ? "" : "Optimal: " + optimalAction);
            DealModel deal = model.getDealModel();
            strategyView.setOptimal(deal.dealerState(), deal.playerState());
        }
    }
    
    private class ShowOptimalListener implements ChangeListener<Boolean> {
        @Override
        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
            strategyView.setShowOptimal(newValue);
        }
        
    }
    
    /**
     * Class that allows set visible/invisible a component by selecting a
     * menu item.
     * Example of usage:
     * {@code
     * Pane xxx = ...;
     * ...
     * xxxMenuItem.selectedProperty().addListener(new CheckMenuItemListener(xxx));
     * }
     */
    private static class CheckMenuItemListener implements ChangeListener<Boolean> {
        final List<Node> nodes = new ArrayList<>();
        CheckMenuItemListener(Node... nodes) {            
            this.nodes.addAll(Arrays.asList(nodes));
        }
        @Override
        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
            nodes.forEach((n) -> {
                n.setVisible(newValue);
            });
        }
    }

    private static class BoolPropToggle implements ChangeListener<Boolean> {
        private final BooleanProperty p;
        BoolPropToggle(BooleanProperty p) {
            this.p = p;
        }
        @Override
        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
            p.set(newValue);
        }
    }
    
    /**
     * Class that allows select a prop value from list in menu.
     */
    private class PropEditListener implements ChangeListener<Toggle> {
        private final Prop prop;
        PropEditListener(Prop p) {
            this.prop = p;
        }
        @Override
        public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {            
            if (newValue == null) {
                return;
            }
            prop.value.set(((RadioMenuItem)newValue).getText());
        }
    }
    
    private class AutoBid implements Runnable {        
        private final GameModel model;
        private final int minBet, maxBet;

        AutoBid(GameModel model) {
            IntegerSpinnerValueFactory f = (IntegerSpinnerValueFactory)betSpinner.getValueFactory();
            minBet = f.getMin();
            maxBet = f.getMax();            
            this.model = model;
        }
        @Override
        public void run() {
            try {
                Thread.sleep(100 * (int)hintView.autoplaySlider.getValue());
                hintView.autoplaySlider.setBackground(Background.EMPTY);
            } catch (InterruptedException e) {
                return;
            }
            Platform.runLater(getAction(model));
        }
        private Runnable getAction(GameModel model) {
            switch (model.getGameState().get()) {
                case DEAL: return () -> {model.userDealAction(model.getDealModel().optimalAction().get());};
                case BET: return () -> {                    
                    int hands = handSpinner.getValue();
                    int bet = model.getOptimalBet(minBet, maxBet);
                    betSpinner.getValueFactory().setValue(bet);
                    model.userBet(bet, hands);
                };
                case SHUFFLE: return () -> {doShuffle();};
                default: return () -> {};
            }
        }
    }
    class CardResizeListener implements ChangeListener<Boolean> {
        @Override
        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
            CardView.setScale(newValue ? 1.2 : 1.6);
            dealerView.resize();
            playerView.resize();
        }        
    }
    
    
    
}
