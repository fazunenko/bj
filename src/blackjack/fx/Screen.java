/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;

import javafx.scene.Scene;

/**
 * 
 * @author Dmitry Fazunenko
 */
enum Screen {
            
    NORMAL("normalSize.css", 1, 1080, 780), SMALL("smallSize.css", 0.2, 850, 625);
    
    static Screen current = NORMAL;
    
    public final String css;
    public final double ratio;
    public final int width;
    public final int height;
    Screen(String css, double ratio, int width, int height) {
        this.css = css;
        this.ratio = ratio;
        this.width = width;
        this.height = height;
    }
    
    public static void updateCSS(Scene s, String cssURL) {
        s.getStylesheets().clear();
        s.getStylesheets().add(cssURL);
    }
}
