/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Class which is responsible for performing actions on GUI change which 
 * require visual effects.
 * 
 * @author Dmitry Fazunenko
 */
class EffectManager {
    static final BooleanProperty USE_EFFECTS = new SimpleBooleanProperty(true);

    /**
     * Basic interface for actions which requires some effect to be played.
     */
    static interface EffectAction {
        Transition effect();
        void beforeEffect();
        void afterEffect();
    }
    
    private static class LateTask implements EffectAction {
        
        final Runnable r;
        LateTask(Runnable r) {
            this.r = r;
        }
        @Override
        public String toString() {
            return "LateTask";
        }

        @Override
        public Transition effect() {
            return null;
        }

        @Override
        public void beforeEffect() {
            r.run();
        }

        @Override
        public void afterEffect() {
        }
        
    }
            
    /**
     * Adds an action to the queue.
     * @param ea action which might require a visual effect.
     */
    static void handle(EffectAction ea) {
        QUEUE.add(ea);
    }
    
    /**
     * Waits until all actions are performed.
     */
    static void runAfter(Runnable r) {
        handle(new LateTask(r));
    }
    
    private static final BlockingQueue<EffectAction> QUEUE = new ArrayBlockingQueue<>(100);
    
    private static class Consumer implements Runnable {
        @Override
        public void run() {
            try {
                while (!(Thread.currentThread().isInterrupted())) {
                    
                    consume(QUEUE.take());
                }
            } catch (InterruptedException ex) {
            }
        }        
    }
    static {
        Thread t = new Thread(new Consumer());
        t.setDaemon(true);
        t.start();
    }
    
    
    private static final Lock LOCK = new ReentrantLock();
    private static boolean isRunning = false;
    private static final Condition inProgress = LOCK.newCondition();
    
    private static void consume(EffectAction action) {
        LOCK.lock();
        isRunning = true;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                LOCK.lock();
                try {
                    Transition effect = action.effect();
                    action.beforeEffect();
                    if (effect != null && USE_EFFECTS.get()) {
                        effect.setOnFinished(new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent event) {
                                LOCK.lock();
                                try {
                                    isRunning = false;
                                    inProgress.signal();
                                } finally {
                                    LOCK.unlock();
                                }
                            }
                        });
                        effect.play();
                    } else {
                        LOCK.lock();
                        try {
                            isRunning = false;
                            inProgress.signal();
                        } finally {
                            LOCK.unlock();
                        }
                    }
                } finally {
                    LOCK.unlock();
                }
            }
        });

        try {
            while (isRunning) {
                inProgress.await();
            }
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    action.afterEffect();
                }
            });
        } catch (InterruptedException e) {
            return;
        } finally {
            LOCK.unlock();
        }
    }
}
