/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */
package blackjack.fx;

import blackjack.model.Strategy;
import blackjack.model.Strategy.Act;
import blackjack.model.Strategy.Acts;
import blackjack.model.Strategy.Dealer;
import blackjack.model.Strategy.IndexType;
import blackjack.model.Strategy.Player;
import blackjack.model.Strategy.StrategyIndex;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.beans.property.ObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.transform.Scale;

/**
 * Display the selected strategy as tabbed pane with the following tabs:
 * <ul>
 * <li> basic view - bare strategy
 * <li> indices - list of indices
 * <li> mixed - basic strategy combined with indices
 * </ul>
 * 
 * @author Dnitry Fazunenko
 */
public class StrategyView extends TabPane implements ChangeListener<Strategy> {
    
    Strategy strategy;
    private boolean showOptimal;
    
    private static final int DEALERS = Dealer.values().length; // just a short cut
    private static final int PLAYERS = Player.values().length; // just a short cut
    private static final Label[][] CELLS = new Label[PLAYERS][DEALERS]; // do hightlight the optimal act
    private final Tab basicTab = new Tab("Basic strategy");
    private final Tab mixedTab = new Tab("Strategy with indices");
    private final Tab indicesTab = new Tab("Indices only");

    @Override
    public void changed(ObservableValue<? extends Strategy> observable, Strategy oldValue, Strategy newValue) {
        updateStrategy(newValue);
    }

    private enum CellType {
        PLAYER_HEAD("black", "white", "PlayerHead"), 
        PLAYER("black", "white", "Player"), 
        DEALER_HEAD("black", "gold", "DealerHead"), 
        DEALER("black", "gold"), 
        LEGEND("black", "white", "Legend"), 
        LEGEND2("black", "white", "Legend2"), 
        LEGEND3("black", "white", "Legend3"), 
        LEGEND4("black", "white", "Legend4"), 
        HIT("black", "olive"), 
        STAND("black", "tomato"), 
        SPLIT("black", "mediumslateblue"),
        DOUBLE("black", "darkorange"),
        DOUBLE_STAND("black", "orange"),
        SURRENDER("black", "peru"),
        SURRENDER_STAND("black", "saddlebrown "),
        CHANGED("red", "lavender"),
        CHANGED_NEG("black", "lavender"),
        CHANGED_ZERO("blue", "lavender"),
        CHANGED_1("green", "lavender"),
        CHANGED_2("yellow", "lavender"),
        CHANGED_3("orange", "lavender"),
        CHANGED_4("red", "lavender"),
        
        INDEX_EMTPY("black","linen"),
        INDEX_NEG("black","powderblue"),
        INDEX_ZERO("black","darkseagreen"),
        INDEX_1("black","yellow"),
        INDEX_2("black","peru"),
        INDEX_3("black","red"),
        INDEX_4("black","fuchsia"),
        INDEX_POS("black","linen"),        
        
        TRANSPARENT("transparent", "transparent", "Normal", "transparent");
        
        public final String fore;
        public final String back;
        public final String width;
        public final String border;
        CellType(String fore, String back) {
            this(fore, back, "Normal", "Black");
        }
        
        CellType(String fore, String back, String width) {
            this(fore, back, width, "Black");
        }
        
        CellType(String fore, String back, String width, String border) {
            this.fore = fore;
            this.back = back;
            this.width = width;
            this.border = border;
        }
    };
    
    private int currentX = -1;
    private int currentY = -1;
        
    public StrategyView() {
        getTabs().addAll(basicTab, indicesTab, mixedTab);
        getStyleClass().add("strategyPane");
        tabClosingPolicyProperty().set(TabClosingPolicy.UNAVAILABLE);
        visibleProperty().addListener(new VisibilityListener());
    }
    
    void setStrategy(ObjectProperty<Strategy> strategyProp) {
        updateStrategy(strategyProp.get());
        strategyProp.addListener(this);
    }
    
    public void updateStrategy(Strategy strategy) {
        this.strategy = strategy;
        if (strategy == null || !isVisible()) {
            return;
        }
        
        basicTab.setContent(strategyTables(strategy.getBasicStrategy()));
        if (strategy.useIndices() == false) {
            mixedTab.setDisable(true);
            indicesTab.setDisable(true);
        } else {            
            mixedTab.setDisable(false);
            mixedTab.setContent(strategyTables(strategy));
            indicesTab.setDisable(false);            
            indicesTab.setContent(indices(strategy.getIndices()));
        }
        
        updateCurrent();
    }
    
    Node strategyTables(Strategy s) {
        GridPane gp = new GridPane();
        
        GridPane[] tables = new GridPane[3];
        tables[0] = createTable(0, Strategy.Player.A2.ordinal(), s);
        tables[1] = createTable(Strategy.Player.A2.ordinal(), Strategy.Player.P2.ordinal(), s);
        tables[2] = createTable(Strategy.Player.P2.ordinal(), PLAYERS, s);

        tables[1].add(createLabel(cell(Act.Hit), cellColor(Act.Hit)), 1, Strategy.Player.P2.ordinal() + 3);
        tables[1].add(createLabel(legend(Act.Hit), CellType.LEGEND2), 2 , Strategy.Player.P2.ordinal() + 3, DEALERS-1, 1);
        tables[1].add(createLabel(cell(Act.Stand), cellColor(Act.Stand)), 4, Strategy.Player.P2.ordinal() + 3);
        tables[1].add(createLabel(legend(Act.Stand), CellType.LEGEND2), 5 , Strategy.Player.P2.ordinal() + 3, DEALERS-1, 1);
        tables[1].add(createLabel(cell(Act.Split), cellColor(Act.Split)), 7, Strategy.Player.P2.ordinal() + 3);
        tables[1].add(createLabel(legend(Act.Split), CellType.LEGEND3), 8, Strategy.Player.P2.ordinal() + 3, DEALERS-1, 1);

        tables[1].add(createLabel(cell(Act.Double), cellColor(Act.Double)), 1, Strategy.Player.P2.ordinal() + 4);
        tables[1].add(createLabel(legend(Act.Double), CellType.LEGEND4), 2, Strategy.Player.P2.ordinal() + 4, DEALERS-1, 1);
        tables[1].add(createLabel(cell(Act.DoubleStand), cellColor(Act.DoubleStand)), 6, Strategy.Player.P2.ordinal() + 4);
        tables[1].add(createLabel(legend(Act.DoubleStand), CellType.LEGEND4), 7, Strategy.Player.P2.ordinal() + 4, DEALERS-1, 1);
        
        if (allowSurrender()) {
            tables[1].add(createLabel(cell(Act.Surrender), cellColor(Act.Surrender)), 1, Strategy.Player.P2.ordinal() + 5);
            tables[1].add(createLabel(legend(Act.Surrender), CellType.LEGEND4), 2, Strategy.Player.P2.ordinal() + 5, DEALERS-1, 1);
            tables[1].add(createLabel(cell(Act.SurrenderStand), cellColor(Act.SurrenderStand)), 6, Strategy.Player.P2.ordinal() + 5);
            tables[1].add(createLabel(legend(Act.SurrenderStand), CellType.LEGEND4), 7, Strategy.Player.P2.ordinal() + 5, DEALERS-1, 1);
        }

        if (s.getIndices() != null) {
            tables[1].add(createLabel("XX", CellType.CHANGED), 1 , Strategy.Player.P2.ordinal() + 6);
            tables[1].add(createLabel("Depends on true count", CellType.LEGEND), 2 , Strategy.Player.P2.ordinal() + 6, DEALERS-1, 1);            
        }

        for (int i = 0; i < tables.length; i++) {
            gp.add(tables[i], i, 0);
        }
        return gp;
    }
    
    private boolean allowSurrender() {
        return strategy.actions(Dealer.DA, Player.H15).agressive == Act.Surrender;
    }
    
    private Node indices(List<StrategyIndex> indices) {
        GridPane gp = new GridPane();
        
        GridPane[] tables = new GridPane[3];
        tables[0] = createIndexTable(indices, IndexType.HARD, IndexType.HARD_DOUBLE);
        tables[1] = createIndexTable(indices, IndexType.SOFT_DOUBLE );
        tables[2] = allowSurrender() ? createIndexTable(indices, IndexType.SPLITS, IndexType.SURRENDER)
                : createIndexTable(indices, IndexType.SPLITS);
        for (int i = 0; i < tables.length; i++) {
            gp.add(tables[i], i, 0);
        }
        return gp;
    }
    
    private GridPane createIndexTable(List<StrategyIndex> indices, IndexType... indexTypes) {
        GridPane table = new GridPane();        
        int row = 0;
        for (IndexType type: indexTypes) {
            Label label = createLabel("Your\nhand", CellType.PLAYER_HEAD);
            table.add(label, 0, row, 1, 2);
            table.add(createLabel(type.kind, CellType.DEALER_HEAD), 1, row, Dealer.values().length, 1);
            row++;
            for (int c = 0; c < DEALERS; c++) {
                table.add(createLabel(title(Dealer.values()[c]), CellType.DEALER), c + 1, row);
            }
            row++;
            for (IndexRow ir: getRows(indices, type)) {
                table.add(createLabel(title(ir.p), CellType.PLAYER), 0, row);
                for (int c = 0; c < DEALERS; c++) {
                    Label lab = createLabel(ir.counts[c], cellIndexColor(ir.counts[c]));
                    table.add(lab, c+1 , row);
                }
                row++;
            }
            //table.add(createLabel("", CellType.TRANSPARENT), 0, row);
            //row++;
        }
        return table;
    }        
    
    private static class IndexRow {
        final Player p;
        final String[] counts;
        IndexRow(Player p, String[] counts) {
            this.p = p;
            this.counts = counts;
        }
    }
    
    List<IndexRow> getRows(List<StrategyIndex> indices, IndexType indexType) {
        Map<Player, String[]> map = new HashMap<>();
        List<IndexRow> list = new ArrayList<>();
        for (StrategyIndex index: indices) {
            if (index.type() != indexType) {
                continue;
            }
            String[] counts = map.get(index.player());
            if (counts == null) {
                counts = new String[Dealer.values().length];
                map.put(index.player(), counts);
                list.add(new IndexRow(index.player(), counts));
            }
            int tc = index.trueCount();
            counts[index.dealer().ordinal()]= tc > 0 && tc < 10 ? "+" + tc : "" + tc;            
        }
        list.sort(new Comparator<IndexRow>() {
            @Override
            public int compare(IndexRow o1, IndexRow o2) {
                return o1.p.ordinal() - o2.p.ordinal();
            }
        });
        return list;
    }
    
    public void setShowOptimal(boolean show) {
        this.showOptimal = show;
        updateCurrent();
    }
    
    public void setOptimal(Dealer d, Player p) {
        if (p != null) {
            currentX = d.ordinal();
            currentY = p.ordinal();
        } else {
            currentX = -1;
            currentY = -1;
        }
        updateCurrent();
    }
                
    private GridPane createTable(int from, int to, Strategy s) {
        GridPane table = new GridPane();        
        Label label = createLabel("Your\nhand", CellType.PLAYER_HEAD);
        table.add(label, 0, 0, 1, 2);        
        table.add(createLabel("Dealer", CellType.DEALER_HEAD), 1, 0, Dealer.values().length, 1);
        for (int c = 0; c < DEALERS; c++) {
            table.add(createLabel(title(Dealer.values()[c]), CellType.DEALER), c + 1, 1);
        }
        
        for (int r = from; r < to; r++) {
            table.add(createLabel(title(Player.values()[r]), CellType.PLAYER), 0, r+2);
            for (int c = 0; c < DEALERS; c++) {
                Acts acts = s.actions(Dealer.values()[c], Player.values()[r]);
                Label lab = createLabel(cell(acts), cellColor(acts));
                CELLS[r][c] = lab;
                table.add(lab, c+1 , r+2);
                lab.setTooltip(tooltip(acts));
            }
        }
        return table;
    }
    private Label createLabel(String txt, CellType type) {
        Label label = new Label(txt);
        label.getStyleClass().add("strategyLabel");
        label.getStyleClass().add("strategyLabel" + type.width);
        label.setStyle(
                "-fx-background-color: " + type.back + 
                "; -fx-border-color: " + type.border +
                "; -fx-text-fill: " + type.fore);
        return label;
    }
    
    Node getLargeStrategyView() {
        Node n = strategyTables(strategy.getBasicStrategy());
        Scale scale = new Scale(1.2, 1.2);
        n.getTransforms().add(scale);
        return n;
    }
    
    Node getLargeIndicesView() {
        Node n = indices(strategy.getIndices());
        Scale scale = new Scale(1.2, 1.2);
        n.getTransforms().add(scale);
        return n;
    }
    
    private static String title(Dealer d) {
        switch(d) {
            case D2: return "2";
            case D3: return "3";
            case D4: return "4";
            case D5: return "5";
            case D6: return "6";
            case D7: return "7";
            case D8: return "8";
            case D9: return "9";
            case DT: return "T";
            case DA: return "A";
        }
        throw new Error("Unknown dealer " + d);
    }
    private static String title(Player h) {
        switch(h) {
            case H7: return "Hard 7-";
            case H8: return "Hard  8";
            case H9: return "Hard  9";
            case H10: return "Hard 10";
            case H11: return "Hard 11";
            case H12: return "Hard 12";
            case H13: return "Hard 13";
            case H14: return "Hard 14";
            case H15: return "Hard 15";
            case H16: return "Hard 16";
            case H17: return "Hard 17";
            case H18: return "Hard 18";
            
            case A2: return "Ace + 2";
            case A3: return "Ace + 3";
            case A4: return "Ace + 4";
            case A5: return "Ace + 5";
            case A6: return "Ace + 6";
            case A7: return "Ace + 7";
            case A8: return "Ace + 8";
            case A9: return "Ace + 9";
            
            case P2: return "(2,2)";
            case P3: return "(3,3)";
            case P4: return "(4,4)";
            case P5: return "(5,5)";
            case P6: return "(6,6)";
            case P7: return "(7,7)";
            case P8: return "(8,8)";
            case P9: return "(9,9)";
            case PT: return "(T,T)";
            case PA: return "(A,A)";
        }
        throw new Error("Unknown hand " + h);
    }
    
    private static String cell(Acts acts) {
        if (acts.isSimple()) {
            return cell(acts.passive);
        } else {
            return cell(acts.passive) + cell(acts.agressive);
        }        
    }

    
    private static String cell(Act a) {
        switch(a) {
            case Hit : return "H" ;
            case Stand : return "S" ;
            case Split : return "P" ;
            case Double : return "D" ;            
            case DoubleStand : return "d" ;
            case Surrender : return "R" ;
            case SurrenderStand : return "r" ;
        }        
        throw new Error("Unknown act " + a);
    }

    private static String legend(Act a) {
        switch(a) {
            case Hit : return "Hit" ;
            case Stand : return "Stand" ;
            case Split : return "Split" ;
            case Double : return "Double(hit)" ;            
            case DoubleStand : return "Dbl(stand)" ;
            case Surrender : return "Surr(hit)" ;
            case SurrenderStand : return "Surr(stand)" ;
        }
        throw new Error("Unknown act " + a);
    }
    
    private static CellType cellColor(Acts acts) {
        if (acts.isSimple()) {
            return cellColor(acts.passive);
        } else {
            if (acts.trueCount < 0) {
                return CellType.CHANGED_NEG;
            } else if (acts.trueCount == 0) {
                return CellType.CHANGED_ZERO;
            } else if (acts.trueCount == 1) {
                return CellType.CHANGED_1;
            } else if (acts.trueCount == 2) {
                return CellType.CHANGED_2;
            } else if (acts.trueCount == 3) {
                return CellType.CHANGED_3;
            } else if (acts.trueCount > 3) {
                return CellType.CHANGED_4;
            }            
            throw new Error("Should never reach this");
        }
    }
    private static CellType cellIndexColor(String s) {
        if (s == null) {
            return CellType.INDEX_EMTPY;
        }
                
        switch(s) {
            case "-3": return CellType.INDEX_NEG;
            case "-2": return CellType.INDEX_NEG;
            case "-1": return CellType.INDEX_NEG;
            case "0": return CellType.INDEX_ZERO;
            case "+1": return CellType.INDEX_1;
            case "+2": return CellType.INDEX_2;
            case "+3": return CellType.INDEX_3;
            case "+4": return CellType.INDEX_4;
            default: return CellType.INDEX_POS;
            
        }
    }
    
    private static Tooltip tooltip(Acts acts) {
        if (acts.isSimple()) {
            return null;
        }
        Tooltip t = new Tooltip(acts.toString());
        t.getStyleClass().add("strategyToolTip");
        return t;        
    }

    private static CellType cellColor(Act a) {
        switch(a) {
            case Hit : return CellType.HIT;
            case Stand : return CellType.STAND;
            case Split : return CellType.SPLIT;
            case Double : return CellType.DOUBLE;            
            case DoubleStand : return CellType.DOUBLE_STAND;
            case Surrender : return CellType.SURRENDER;
            case SurrenderStand : return CellType.SURRENDER_STAND;
        }
        throw new Error("Unknown act " + a);
    }
    
    private void updateCurrent() {
        if (!isVisible()) {
            return;
        }
        for (int r = 0; r < PLAYERS; r++ ) {
            for (int c = 0; c < DEALERS; c++) {
                boolean isCurrent = currentX == c && currentY == r;
                CELLS[r][c].setDisable(showOptimal && !isCurrent);
            }
        }        
    }    
    
    class VisibilityListener implements ChangeListener<Boolean> {
        @Override
        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
            updateStrategy(strategy);
        }        
    }
}
