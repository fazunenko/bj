/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;

/**
 * View of the dealer hand.
 * 
 * @author Dmitry Fazunenko
 */
public class DealerHandView extends HandPane {

    @Override
    public boolean showSoftPoints() {
        return true;
    }
    
    @Override
    boolean isDealer() {
        return true;
    }
    
}
