/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;

import blackjack.model.Card;
import javafx.animation.FadeTransition;
import javafx.animation.Transition;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 * View of a single card.
 * 
 * @author Dmitry Fazunenko
 */
public class CardView extends ImageView implements InvalidationListener {
    
    private static double scale = 1;
    final Card card;
    static final int W = 71;
    static final int H = 96;
    private static final Image IMG = new Image("blackjack/fx/allcards.gif");
    private static final Image BACK = new Image("blackjack/fx/back.gif");
    
    public final int visibleID;
    private boolean isPlaying;
    
    public CardView(Card card) {
        if (!card.isOpen()) {
            super.setImage(BACK);
            card.addListener(this); // to be notified when card becomes open
        } else {
            super.setImage(IMG);
            Rectangle2D viewportRect = new Rectangle2D(W*shift(card), 0, W, H);
            super.setViewport(viewportRect);
        }
        this.card = card;
        visibleID = card.visibleID();
        super.setFitHeight(hight());
        super.setFitWidth(width());
        super.setSmooth(true);
        super.setCache(true);
        super.setPreserveRatio(true);
    }
    
    public static void setScale(double s) {
        scale = s;
    }    
    public static double width() {
        return scale * W;
    }
    
    public static double hight() {
        return scale * H;
    }
        
    private static int shift(Card c) {
        int s = c.suite;
        int r = 12 - (c.rank + 12) % 13;
        return 13*s + r;
    }
    
    synchronized void setPlaying(boolean f) {
        isPlaying = f;
        notifyAll();
    }
    synchronized boolean isPlaying() {
        return isPlaying;
    }
    
    void open() {        
        EffectManager.handle(new OpenCard(this, true));
        EffectManager.handle(new OpenCard(this, false));
    }

    @Override
    public void invalidated(Observable observable) {        
        if (card.isOpen()) {
            open();
        }
    }

    static class OpenCard implements EffectManager.EffectAction {
        
        private final CardView cv;
        private final Transition effect;
        private final boolean isFirst;
        
        OpenCard(CardView cv, boolean isFirst) {
            this.cv = cv;
            effect = createEffect(isFirst);
            this.isFirst = isFirst;
        }
        
        @Override
        public String toString() {
            return "Open card begin " + cv.card + " " + isFirst;
        }

        @Override
        public Transition effect() {
            return effect;
        }

        private Transition createEffect(boolean isFirst) {
            FadeTransition t = new FadeTransition(Duration.millis(100), cv);
            if (isFirst) {
                t.setFromValue(1);
                t.setToValue(0);
            } else {
                t.setFromValue(0);
                t.setToValue(1);
            }
            return t;
        }

        @Override
        public void beforeEffect() {
        }

        @Override
        public void afterEffect() {
            if (isFirst) {
                cv.setImage(IMG);
                cv.setViewport(new Rectangle2D(W*shift(cv.card), 0, W, H));            
            }
        }        
    }
    
}
