/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import blackjack.game.Game;
import blackjack.model.GameModel;
import blackjack.model.GameRules;
import blackjack.model.GameStatistics;
import java.io.File;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;

/**
 * Entry point.
 * 
 * @author Dmitry Fazunenko
 */
public class Main extends Application {

    public static Stage primaryStage;
    public static GUIController controller;
    public static boolean isTest = false;
    

    @Override
    public void start(Stage stage) throws Exception {
        primaryStage = stage;
        detectScreen();
        stage.setTitle("BJ simulator");        
        stage.getIcons().add(new Image("blackjack/fx/background.jpg"));
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResource("GUI.fxml"));        
        Scene scene = new Scene(root, Screen.current.width, Screen.current.height);
        stage.setScene(scene);
        stage.show();
        if (!isTest) {
            GameModel m = new Game();
            controller.setModel(m);
            m.begin();
        } else {
            System.out.println("Selftest mode");
        }
    }
    
    private void detectScreen() {
        Rectangle2D r = javafx.stage.Screen.getPrimary().getVisualBounds();
        Screen.current = r.getWidth() >= Screen.NORMAL.width && r.getHeight() >= Screen.NORMAL.height ?
                Screen.NORMAL : Screen.SMALL;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String... args) throws Exception  {
        isTest = (args.length > 0 && "selftest".equalsIgnoreCase(args[0]));
        if (!isTest) {
            launch(args);
        } else {
            // generate fr files for further testing
            String [][] testRules = {
                //g.getShoe().setShuffledCards("Ac Qh Ts Jc Ah 5c Kc 6h 3s Ac 6d 8c Jc 8c 7c 4c Qh 3h 8s Ac 2h 6d 5s 3s 7s 5c 6c 4d 2s 5s Ah 4s 7h 2s 7h 6s 3c As Qd 7c 8s Ac 5d Ts Qh 9s Tc 4s Ac Ks 9s As 2c 4s 8s 6s Qh 5s 8c 9c 4d 3d 6d 5h Ah 3c 6s Jd 8s Kd 3h 5d 9s Ad 3s Jc 2h As 9h Qs Qd 2d 2h Qs Qd Qs 9s Jh Jd 4c Qs Jc 2d Ts 8h Jh Kd 4c Kc 8h Qd Kc Kd 6d 9d 6s Qd 2c 4d 7c 3s 3c Js Th Ah 4c 5d Jd 6c 5c 3d 4h Tc 5h 7s Td 8d 6s Th 9c Kh 6c 7d 3d Ts 6h 9d 4h 8d 3h Qs 6h 5c As 6s 5h 8c 9c Jd Ks 3h 2h Ad 6h 3s 5h Jd Qc Ad 7d Ks Ad Js Ks 9h Jd 7h 9d Kc 9s 6h 2c Td Qh 9s 8h Ts Ad Ad 8h 3h 5s 6c 9h 4d Th 8d As Jh 2d 7c Td 8h 6c Ah 2c Qc 7s Js Ah 8d 2h 2d 5s 9d Kh 4d 5c Qd 8h Ac 9c Th 2h 4s Qh Tc Ks 5d 2s Kd Jh 4h Th 9h Jh 3c Qc 7s Qc 5d 7s 4h 2s 3c 9h 4c 9d 4h 6d Ks Jh Kc 3c Qc 7h 8s 4c 7d 7d Kd 7d Js 5h 6d Ts 9c Qs 3d Kh 7d 8d 4d 8c 5s 4s Jc 3h Tc Qc Kh 7s Td Tc 5h As 2c Jc Kh Td 3d 2d 9d 8d Js 6h Kh 2d 4h 8s Kd 5d Kc 2c 6c 3s Td 3d 7h 8c 7c Th 2s Tc 5c 9c Js 7c 9h 7h 4s 2s");                
                //{"Decks#='#decks 6'; DealerBJ='Dealer BJ: peek';    Surrender='No surrender'",       "d6_p_ns.fr"},
                //{"Decks#='#decks 6'; DealerBJ='Dealer BJ: no peek'; Surrender='No surrender'",       "d6_np_ns.fr"},
                //{"Decks#='#decks 6'; DealerBJ='Dealer BJ: peek';    Surrender='Allow surrender'",    "d6_p_as.fr"},
                //{"Decks#='#decks 6'; DealerBJ='Dealer BJ: no peek'; Surrender='Allow surrender'",    "d6_np_as.fr"},
                
                //g.getShoe().setShuffledCards("3d 2d 4d 7c 8h Ad Td 2s Ad");                
                //{"Decks#='#decks 2'; Insurance='Suggest insurance'",   "d2_insurance_yes.fr"},
                //{"Decks#='#decks 2'; Insurance='Reject insurance'",   "d2_insurance_no.fr"},
                // {"Decks#='#decks 2'; BetMethod='Bet pro rata'",  "d2_bet_pro_rata.fr"},
                // {"Decks#='#decks 2'; BetMethod='Bet minimum'",   "d2_bet_min.fr"},
                // {"Decks#='#decks 2'; BetMethod='Bet maximum'",   "d2_bet_max.fr"},

                // g.getShoe().setShuffledCards("3h 9h 8d 8c 6s 8c Qd 6d 4c 2h 3d 4d 2d 8s 2c 2d 5h 4h 5d 7h 3d Td Kd 3s 7s 5s Jh Jh 5c 4h 4h 4h 4h 4h 4h 4h 4h 4h 4h 4h 9d Ac Jd 8c Ah 3s Th 8h 7h 4s Kd 7d 2d Js 8d 3c 9h Kd 7s 3c 2s Ah 3d 5c 7h 3h Ah 4d 6s 7c 4s 2c 2c 7s 2h Qh 9c 5s 6h 6c 5h Qs Th 5d Ad 2s 3s 3d 7h 7d 2c Ts 2s Jc Ad Jc 6c 4d Ad 8d 5c 6s 2s Kh 2c 5h 8s 4d 6s Qc 9c As 9c 6h 8d 4c 4h 6d 4d 4h Qd 3s Ah 6h Th Qh 4s 9c 9h Ac 2h 3h 9s 3h Td 5h Qh 6h Ks 2h 8s Ks Qd 4h");                                
                {"Decks#='#decks 6'; Strategy='With indices'",       "d6_indices_yes.fr"},
                {"Decks#='#decks 6'; Strategy='Basic'",              "d6_indices_no.fr"},
            };
            
            for (int i = 0; i < testRules.length; i++) {
                Game g = new Game();                
                g.getShoe().setShuffledCards("3h 9h 8d 8c 6s 8c Qd 6d 4c 2h 3d 4d 2d 8s 2c 2d 5h 4h 5d 7h 3d Td Kd 3s 7s 5s Jh Jh 5c 4h 4h 4h 4h 4h 4h 4h 4h 4h 4h 4h 9d Ac Jd 8c Ah 3s Th 8h 7h 4s Kd 7d 2d Js 8d 3c 9h Kd 7s 3c 2s Ah 3d 5c 7h 3h Ah 4d 6s 7c 4s 2c 2c 7s 2h Qh 9c 5s 6h 6c 5h Qs Th 5d Ad 2s 3s 3d 7h 7d 2c Ts 2s Jc Ad Jc 6c 4d Ad 8d 5c 6s 2s Kh 2c 5h 8s 4d 6s Qc 9c As 9c 6h 8d 4c 4h 6d 4d 4h Qd 3s Ah 6h Th Qh 4s 9c 9h Ac 2h 3h 9s 3h Td 5h Qh 6h Ks 2h 8s Ks Qd 4h");                                
                GameRules rules = g.getRules();
                System.out.println("=== " + testRules[i][0]);
                rules.fromString(testRules[i][0]);
                g.setRecording(new File(testRules[i][1]));
                g.begin();
                Game.playShuffle(g, 1);
            }
            
            System.exit(0);
        }
    }

}
