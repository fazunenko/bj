/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;

import javafx.scene.Node;
import javafx.scene.control.Alert;

/**
 * Basic class for dialogs.
 * 
 * @author Dmitry Fazunenko
 */
class BJDialog extends Alert {
    private Node main;
    public BJDialog(AlertType alertType) {
        super(alertType);
    }
    void setMainPane(Node p) {
        main = p;
    }
    
    void displayAndWait() {
        main.setDisable(true);
        showAndWait();
        main.setDisable(false);
    }
}
