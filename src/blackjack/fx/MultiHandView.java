/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;

import blackjack.model.HandModel;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.layout.HBox;

/**
 * View of a multiple player hands.
 * 
 * @author Dmitry Fazunenko
 */
public class MultiHandView extends HBox implements ListChangeListener<HandModel>, ChangeListener<Number> {
    
    private ObservableList<? extends HandModel> hands;
    private final List<PlayerHandView> hvList = new ArrayList<>();
    private IntegerProperty currentHandNum;
    
    /**
     * Connects view and the model (list of player hands).
     * @param hands list of hand to observe
     * @param currentHandNum current active hand
     */
    public void setHands(ObservableList<? extends HandModel> hands, 
            IntegerProperty currentHandNum) {
        
        if (this.hands != null) {
            hvList.clear();
            getChildren().clear();
            this.hands.removeListener(this);
            this.currentHandNum.removeListener(this);
        }
        this.currentHandNum = currentHandNum;
        currentHandNum.addListener(this);
        this.hands = hands;
        hands.addListener(this);

        hands.forEach((hm) -> {
            add(hm);
        });
        updateVisibleCurrent();
    }
    void resize() {
        // setHands(hands, currentHandNum);
        for (PlayerHandView hv: this.hvList) {
            hv.resize();
        }
    }

    /**
     * Reaction on adding/removing a player hand.
     * @param c the change to handle
     */
    @Override
    public void onChanged(ListChangeListener.Change<? extends HandModel> c) {
        while (c.next()) {
            c.getAddedSubList().forEach(hm -> {
                add(hm);
            });
            c.getRemoved().forEach(hm -> {
                remove(hm);
            });

        }
        updateVisibleCurrent();
    }
    
    private void add(HandModel hm) {
        PlayerHandView hv = new PlayerHandView();
        hv.setHand(hm);
        getChildren().add(hm.number(), hv);
        hvList.add(hm.number(), hv);
    }
    
    private void remove(HandModel hm) {
        PlayerHandView hv = searchHandView(hm);
        if (hv != null) {
            hvList.remove(hv);
            getChildren().remove(hv);            
        }
    }
    
    private PlayerHandView searchHandView(HandModel hm) {
        for (PlayerHandView hv: hvList) {
            if (hv.handModel() == hm) {
                return hv;
            }
        }
        return null;
    }
    
    private void updateVisibleCurrent() {
        int n = currentHandNum.get();
        for (int i = 0; i < hvList.size(); i++) {
            hvList.get(i).setActive(n == i || n < 0);
        }
    }

    public void fadeDown() {
        hvList.forEach((hv) -> {
            hv.fadeDown();
        });        
    }
    public void fadeUp() {
        hvList.forEach((hv) -> {
            hv.fadeUp();
        });        
    }

    @Override
    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
        updateVisibleCurrent();
    }
}
