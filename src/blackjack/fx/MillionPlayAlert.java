/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;

import blackjack.model.GameStatistics;
import java.util.List;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Dialog to display million shuffle playing.
 * 
 * @author Dmitry Fazunenko
 */
class MillionPlayAlert extends BJDialog {    
    private final ObservableList<Node> kids;
    private final ProgressBar progressBar;
    private final Text percentText;
    MillionPlayAlert() {
        super(AlertType.INFORMATION);
        super.setHeaderText("Evaluation of the selected strategy on 3 000 000 shuffles");
        VBox pane = new VBox();
        kids = pane.getChildren();
        progressBar = new ProgressBar();        
        progressBar.getStyleClass().add("Spinner");
        percentText = new Text();
        percentText.getStyleClass().add("textTotal");
        getDialogPane().setContent(pane);
        //getDialogPane().getStylesheets().add(getClass().getResource("normalSize.css").toExternalForm());
    }    
    
    void display(Task<GameStatistics> task, List<String> props) {
        kids.clear();
        props.forEach((p) -> {
            Text prop = new Text(p);
            prop.getStyleClass().add("textDialog");
            kids.add(prop);
        });
        kids.add(percentText);
        kids.add(progressBar);
        
        progressBar.setVisible(true);
        progressBar.progressProperty().bind(task.progressProperty());
        percentText.textProperty().bind(task.messageProperty());
        progressBar.setMinWidth(200);
        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                progressBar.setVisible(false);
            }            
        });        
        super.displayAndWait();
    }
    
}
