/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;
/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

import blackjack.model.Shoe;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Rotate;

/**
 * Pane to display the shoe and the game hints.
 *
 * @author Dmitry Fazunenko
 */
public class HintView extends VBox implements InvalidationListener {
    private Shoe shoe;
    // private final ImageView backImage = createBackImage();
    final Text shoeText = createText("textShoe");
    private final StackPane shoeLabel = new StackPane();
    private final ProgressBar shoeProgress = new ProgressBar();
    private final ProgressBar shoeProgressFull = new ProgressBar();
    
    final Text runningCountText = createText("textRules");
    final Text trueCountText = createText("textRules");
    final Button toggleAutoplayButton;
    final Text optimalActionText = createText("textRules"); // TODO: move to another view
    final Text sliderText = createText("textRules");
    final Slider autoplaySlider;
    
    public HintView() {
        super();
        super.getStyleClass().add("middle");
        ObservableList<Node> kids = getChildren();
        HBox shoeView = new HBox(); // card back + progress bar
        HBox pile = new HBox(); // two progress bars
        pile.getChildren().addAll(shoeProgressFull, shoeProgress);
        kids.add(shoeView);
        shoeView.getChildren().addAll(shoeLabel, pile);
        shoeLabel.getChildren().addAll(createBackImage(), shoeText);
        shoeLabel.setPrefWidth(CardView.W - 30);

        shoeProgress.setProgress(1);
        shoeProgressFull.setProgress(1);
        shoeProgress.setPrefHeight(CardView.H);
        shoeProgress.setPrefWidth(CardView.H / 2);
        shoeProgressFull.setProgress(1);
        shoeProgressFull.setPrefHeight(CardView.H);
        shoeProgressFull.setPrefWidth(CardView.H / 2);
        pile.getTransforms().setAll(new Rotate(90,CardView.H / 2,CardView.H / 2));
        
        
        kids.add(runningCountText);
        kids.add(trueCountText);
        kids.add(optimalActionText);
        
        toggleAutoplayButton = new Button("Start autopay");
        toggleAutoplayButton.setTextAlignment(TextAlignment.CENTER);
        toggleAutoplayButton.getStyleClass().add("textRules");
        kids.add(toggleAutoplayButton);
        
        sliderText.setText("pause:");
        kids.add(sliderText);
        autoplaySlider = new Slider();
        autoplaySlider.setMin(0);
        autoplaySlider.setMax(100);
        autoplaySlider.setShowTickMarks(true);
        autoplaySlider.setShowTickLabels(true);
        autoplaySlider.setValue(25);
        kids.add(autoplaySlider);
    }
    
    
    void setShoe(Shoe shoe) {
        if (this.shoe == shoe) {
            return;
        }
        if (this.shoe != null) {
            this.shoe.removeListener(this);        
        }
        this.shoe = shoe;
        this.shoe.addListener(this);
    }

    private ImageView createBackImage() {
        ImageView iv = new ImageView();
        iv.setImage(new Image("blackjack/fx/back.gif"));
        iv.setSmooth(true);
        iv.setPreserveRatio(true);
        return iv;
    }
    
            
    /**
     * Reaction on changes to the Shoe
     */
    @Override
    public final void invalidated(Observable observable) {
        Shoe d = (Shoe)observable;            
        shoeText.setText("" + (d.total() - d.played()));
        
        int left = d.total() - d.played();
        int half = d.total() / 2;
        shoeProgress.setProgress(left > half ? (left - half) / (float)half : 0);
        //shoeProgressFull.setProgress(left > half ? 1 : half / (float)left);
        
        
        trueCountText.setText("True counts: " + d.getTrueCount());
        runningCountText.setText("Running counts: " + d.getRunningCount());    
        //shoeProgressFull.setPrefWidth(shoe.total() / 4);
        //shoeProgress.setPrefWidth(shoe.total() / 4);
    }        

    private Text createText(String style) {
        Text txt = new Text();
        txt.setTextAlignment(TextAlignment.CENTER);
        txt.getStyleClass().add(style);
        return txt;
    }

    
    @Override
    protected double computePrefWidth(double height) {
        return 200;
    }

    @Override
    protected double computePrefHeight(double height) {
        return 300;
    }
}