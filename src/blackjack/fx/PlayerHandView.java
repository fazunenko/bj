/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;

/**
 * View of a player hand.
 * 
 * @author Dmitry Fazunenko
 */
public class PlayerHandView extends HandPane {

    /**
     * Reaction on event when a hand becomes active/not-active.
     * Active hands will be faded up, non active - down.
     * 
     * @param isActive
     */
    void setActive(boolean isActive) {
        if (isActive) {
            squize(false);
            fade(1);
        } else {
            squize(true);
            fade(0.3);
        }
    }
    
    @Override
    protected double computePrefWidth(double height) {
        return 1.2 * super.computeMinWidth(height);
    }
    
    @Override
    boolean isDealer() {
        return false;
    }
    
}
