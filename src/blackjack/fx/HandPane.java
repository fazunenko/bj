/*
 * Copyright (c) 2016, 2017, Dmitry Fazunenko and/or his affiliates. 
 * All rights reserved.
 */

package blackjack.fx;

import blackjack.fx.EffectManager.EffectAction;
import blackjack.model.Card;
import blackjack.model.HandModel;
import java.util.ArrayList;
import java.util.List;
import javafx.animation.Animation.Status;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;

/**
 * Pane to display a set of cards and their points. Implements own layout.
 *
 * It's supposed to have two subclasses implementing dealer and player specifics.
 *
 * @author Dmitry Fazunenko
 */
abstract class HandPane extends Region implements ListChangeListener<Card>, ChangeListener<Number>, InvalidationListener {

    private HandModel hand = null;
    static final BooleanProperty SHOW_POINTS = new SimpleBooleanProperty(true);
    
    // GUI 
    private final Text points = new Text();
    private final Text result = new Text();
    private final Text lastBid = new Text();
    private final List<CardView> cvList = new ArrayList<>();
    private final ObservableList<Node> kids = getChildren();
    
    
    // Layout contstants    
    private final static double H_GAP = 20;
    private final static double V_GAP = 5;
    private final static double H_POINTS = 30;
    private final static double SHIFT_NORMAL = -0.5;
    private final static double SHIFT_SQUIZED = -0.7;
    
    // fade effects
    private final FadeTransition FADE_HAND = new FadeTransition(Duration.millis(500), this);
    //private final FadeTransition FADE_RESULT = new FadeTransition(Duration.millis(500), result);
    private final FadeTransition FADE_LAST_BID = new FadeTransition(Duration.millis(1000), lastBid);
    private double fade = 0;
    private double shift = SHIFT_NORMAL * CardView.width();
        
    /**
     * Connects view and model.
     *
     * Supposed to be called only once per view instance.
     *
     * @param hm hand to view
     */
    public void setHand(HandModel hm) {
        if (this.hand != null) {
            hand.removeListener(this);
            hand.result().removeListener((ChangeListener)this);
            hand.cards().removeListener((ListChangeListener)this);            
            result.setText("");                        
            cvList.clear();
            kids.clear();
        }
        this.hand = hm;
        hm.addListener(this);
        hm.result().addListener((ChangeListener)this);        
        hm.cards().addListener((ListChangeListener)this);
        hand.lastBid().addListener(new LastBidListener());
        points.getStyleClass().add("text");
        points.visibleProperty().bind(SHOW_POINTS);
        result.getStyleClass().add("textStatus");
        lastBid.getStyleClass().add("text");
        for (int i = 0; i < hm.cards().size(); i++) {
            CardView cv = new CardView(hm.cards().get(i));
            cvList.add(cv);
            EffectManager.handle(new DealCard(cv, kids, isDealer(), hm.isAfterSplit()));
        }
        kids.add(points);
        kids.add(result);
        kids.add(lastBid);
        EffectManager.handle(new SetText(points, pointText()));
    }

    /**
     * @return true if soft points should be displayed.
     */
    boolean showSoftPoints() {
        if (hand == null) {
            return false;
        }
        for (Card c: hand.cards()) {
            if (!c.isOpen()) {
                return false;
            }
        }
        return true;
    }
    abstract boolean isDealer();
    
    HandModel handModel() {
        return hand;
    }
    
    void resize() {
        kids.removeAll(cvList);
        List<CardView> newLabels = new ArrayList<>();
        cvList.stream().forEach((cv) -> {
            newLabels.add(new CardView(cv.card));
        });
        cvList.clear();
        for (CardView cv: newLabels) {
            cvList.add(cv);
            kids.add(kids.size() - 2, cv);
        }
        
    }

    @Override
    protected double computeMinWidth(double height) {
        int num = hand == null ? 2 : hand.cards().size();
        return num * CardView.width() + (num - 1) * shift + H_GAP * 2 * Screen.current.ratio;        
    }

    @Override
    protected double computePrefHeight(double height) {
        return CardView.hight() + V_GAP * 2 * Screen.current.ratio + H_POINTS;
    }

    @Override
    protected void layoutChildren() {
        int n = 0;
        for (CardView cv : cvList) {
            cv.setLayoutX(H_GAP + n * (CardView.width() + shift));
            cv.setLayoutY(V_GAP);
            n++;
        }
        double width = computePrefWidth(0);
        double height = computePrefHeight(0);
        
        double ph = (H_POINTS - points.prefHeight(0)) / 2;
        points.setLayoutX((width - points.prefWidth(0)) / 2);
        //points.setLayoutY(height - vGap - pointsH + ph);
        points.setLayoutY(height - V_GAP);

        result.setLayoutX((width - result.prefWidth(0)) / 2);
        //result.setLayoutY((height - result.prefHeight(0)) / 2);
        result.setLayoutY(H_POINTS + CardView.hight() / 2);

        lastBid.setLayoutX((width - lastBid.prefWidth(0)) / 2);
        //points.setLayoutY(height - vGap - pointsH + ph);
        lastBid.setLayoutY(0);
    }

    /**
     * Defines reaction on adding/removing cards.
     * @param c the change to the list of cards under observation.
     */
    @Override
    public void onChanged(Change<? extends Card> c) {
                int mSize = hand.cards().size();
        int vSize = cvList.size();
        if (mSize == 0) {
            if (vSize == 0) {
                // nothing to do
            } else {
                kids.removeAll(cvList);
                cvList.clear();                
            }
        } else {
            if (mSize == vSize) {
                // some cards was open
            } else if (mSize > vSize) {
                for (int i = vSize; i < mSize; i++) {
                    CardView newCV = new CardView(hand.cards().get(i));                    
                    cvList.add(newCV);
                    EffectManager.handle(new DealCard(newCV, kids, isDealer(), false));
                }
            } else {
                for (int i = vSize - 1; i >= mSize; i--) {
                    CardView rmCV = cvList.get(i);
                    kids.remove(rmCV);
                    cvList.remove(rmCV);
                }
            }            
        }       
        EffectManager.handle(new SetText(points, pointText()));        
    }
    
    
    /**
     * @return text to be displayed in the points.
     */
    private String pointText() {
        int p = hand.points();
        int sp = hand.softPoints();
        switch (hand.state()) {
            case INSURANCE:
                return "Insurance?";
            case BLACK_JACK:
                return "Black Jack";
            case BUST:
                return "Busted: " + p;
            case JUST:
                return "21!";
            case SURRENDER:
                return "Surrender";
        }
        if (p == 0) {
            return "";
        } else if (showSoftPoints() && p > sp) {
            return sp + "/" + p;
        } else {
            return "" + p;
        }
    }
    
    /**
     * Defines reaction on changes the deal status.
     * 
     * @param ov 
     * @param old previous result value
     * @param nv new result value
     */
    @Override
    public void changed(ObservableValue<? extends Number> ov, Number old, Number nv) {
        int newValue = nv.intValue();
        Color c = null;
        String str = "" + newValue;
        if (newValue == Integer.MAX_VALUE) {
            // deal just started
            //result.setText("");
            str = "";
        } else if (newValue < 0) {
            //result.setFill(Color.RED);
            c = Color.RED;
        } else if (newValue > 0) {
            //result.setText("+" + newValue);
            //result.setFill(Color.GREEN);
            str = "+" + newValue;
            c = Color.GREEN;
        } else {
            // result.setFill(Color.YELLOW);
            c = Color.YELLOW;
        }
        EffectManager.handle(new SetText(result, str, c, true));
    }
    @Override
    public void invalidated(Observable observable) {
        EffectManager.handle(new SetText(points, pointText()));
    }
    

    public void fadeUp() {
        fade(1);
    }

    public void fadeDown() {
        fade(0);
    }

    void fade(double to) {
        if (FADE_HAND.getStatus() == Status.RUNNING) {
            FADE_HAND.stop();
        }
        FADE_HAND.setFromValue(fade);
        FADE_HAND.setToValue(to);
        FADE_HAND.setCycleCount(1);
        FADE_HAND.play();
        fade = to;
    }
    
    void squize(boolean isSquized) {
        shift = isSquized ? CardView.width() * SHIFT_SQUIZED : CardView.width() * SHIFT_NORMAL;
        requestLayout();
    }

    private class LastBidListener implements InvalidationListener {

        public LastBidListener() {
        }

        @Override
        public void invalidated(Observable observable) {
            String value = ((StringProperty)observable).get();
            if (value != null) {
                lastBid.setText(value);
                FADE_LAST_BID.setFromValue(1);
                FADE_LAST_BID.setToValue(0);
                FADE_LAST_BID.play();
            }
        }
    }
        
    static class SetText implements EffectAction {
        private final Text txt;
        private final String str;
        private final Color c;
        private final Transition effect;
        
        SetText(Text txt, String str) {
            this(txt, str, null, false);
        }
        
        SetText(Text txt, String str, Color c, boolean fade) {
            this.txt = txt;
            this.str = str;
            this.c = c;
            this.effect = fade ? createEffect() : null;
        }
        
        private Transition createEffect() {
            FadeTransition t = new FadeTransition(Duration.millis(500), txt);
            t.setFromValue(0);
            t.setToValue(1);
            return t;
        }

        @Override
        public Transition effect() {
            return effect;
        }

        @Override
        public void beforeEffect() {
            txt.setText(str);
            if (c != null) {
                txt.setFill(c);
            }
        }

        @Override
        public void afterEffect() {
        }
        
        @Override
        public String toString() {
            return "setText " + str;
        }
    }
    
    static class DealCard implements EffectAction {
        
        private final CardView cv;
        private final Transition effect;
        private final ObservableList<Node> kids;
        
        DealCard(CardView cv, ObservableList<Node> kids, boolean isDealer, boolean isAfterSplit) {
            this.cv = cv;
            this.kids = kids;
            effect = createEffect(isDealer, isAfterSplit);
        }
        
        @Override
        public String toString() {
            return "Deal " + cv.card;
        }

        @Override
        public Transition effect() {
            return effect;
        }

        private Transition createEffect(boolean isDealer, boolean afterSplit) {
            TranslateTransition translateTransition =
                new TranslateTransition(Duration.millis(300), cv);
            if (!afterSplit) {
                translateTransition.setFromX(500);
                translateTransition.setToX(0);
            } else {
                translateTransition.setFromX(-100);
                translateTransition.setToX(0);
            }
            if (!isDealer && !afterSplit) {
                translateTransition.setFromY(-300);
                translateTransition.setToY(0);
            }                        
            translateTransition.setCycleCount(1);
            translateTransition.setAutoReverse(false);
            ScaleTransition scaleTransition = 
                new ScaleTransition(Duration.millis(300), cv);
            scaleTransition.setFromX(0f);
            scaleTransition.setToX(1f);
            scaleTransition.setFromY(0f);
            scaleTransition.setToY(1f);
            scaleTransition.setCycleCount(1);
            scaleTransition.setAutoReverse(false);

            ParallelTransition parallelTransition = new ParallelTransition();            
            parallelTransition.getChildren().add(translateTransition);
            if (!afterSplit) {
                parallelTransition.getChildren().add(scaleTransition);
            }
            parallelTransition.setCycleCount(1);      
            return parallelTransition;            
        }

        @Override
        public void beforeEffect() {
            //cvList.add(cv);
            kids.add(kids.size() - 2, cv);
        }

        @Override
        public void afterEffect() {
        }        
    }
    
}
