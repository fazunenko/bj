/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.model;

import java.util.Random;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author Fa
 */
public class DeckNGTest {
    
    public DeckNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }


    /**
     * Test general behavior
     */
    @Test
    public void testGeneral() {
        System.out.println("nextCard");
        int deckCount = 5;
        int ranks[] = new int[13];
        int suits[] = new int[4];
        Shoe deck = new Shoe();
        deck.shuffle(deckCount);
        assertEquals(deck.total(), deckCount * Shoe.LEN, " total()");
        for (int i = 0; i < deckCount * Shoe.LEN; i++) {
            boolean isOpen = (i % 2 == 0);
            Card c = isOpen ? deck.nextCard() : deck.nextClosedCard();
            assertEquals(c.isOpen(), isOpen, " card.isOpen() " + i);
            assertEquals(deck.played(), i + 1,  " deck.played() " + i);
            ranks[c.rank]++;
            suits[c.suite]++;
        }
        for (int i = 0; i < ranks.length; i++) {
            assertEquals(ranks[i], 4*deckCount, " ranks count " + i);
        }
        for (int i = 0; i < suits.length; i++) {
            assertEquals(suits[i], 13*deckCount, " suite count " + i);
        }
        
        try {
            Card overLast = deck.nextCard();
            fail("No cards should left by that moment " + overLast);
        } catch (Error e) {            
        }
        
    }
    
    @Test
    public void testTrueCount() {
        
        Shoe deck = new Shoe();
        int cards[] = {0, 6, 2, 7, 11, 12, 4, 5, 8, 10, 9,  2,
                       0, 1, 2, 4,  5,  6, 7, 8, 9, 10, 11, 12};
        deck.setShuffledCards((int[])cards);
        
        for (int d = 1; d < 6; d++) {
            deck.shuffle(d);
            
            deck.nextCard();
            deck.nextCard();
            deck.nextCard();
            deck.nextClosedCard();
            int r1 = deck.getRunningCount();
            assertEquals(r1, 0, "running, after 4(1) cards" + d);
            assertEquals(deck.getTrueCount(), 0.0f, "true after 4(1)" + d);
            deck.nextCard();
            deck.nextClosedCard();
            
            int r2 = deck.getRunningCount();
            assertEquals(r2, -1, "running, after 6(2) cards" + d);
            assertEquals(deck.getTrueCount(), expectedTrue(d, r2, 4) , "true after 6(2) cards, decks: " + d);
            deck.endDeal();
            int r3 = deck.getRunningCount();
            assertEquals(r3, -2, "running, after 6(0) cards" + d);
            assertEquals(deck.getTrueCount(), expectedTrue(d, r3, 6) , "true after 6(2) cards, decks: " + d);
            
            deck.nextCard();
            int r4 = deck.getRunningCount();
            assertEquals(r4, -1, "running, after 7(0) cards" + d);
            assertEquals(deck.getTrueCount(), expectedTrue(d, r4, 7) , "true after 7(0) cards, decks: " + d);
            
            
            deck.endDeal();
        }
    }
    
    
    @Test(dataProvider="oneIndexDecks")
    public void testRunningCount(Object cards, Object index) {
        
        int in = (int)index;
        Shoe deck = new Shoe();
        deck.setShuffledCards((int[])cards);
        
        for (int d = 1; d < 6; d++) {
            deck.shuffle(d);
            for (int i = 0; i < 20; i++) {
                deck.nextCard();
                deck.nextClosedCard();
            }
            int r = deck.getRunningCount();
            assertEquals(r, in * 20, "running count, when half is closed");
            assertEquals(deck.getTrueCount(), expectedTrue(d, r, 20), "true cards, when half is closed");
            deck.endDeal();
            int r2 = deck.getRunningCount();
            assertEquals(r2, 2*r, "running count, when all open");
            assertEquals(deck.getTrueCount(), expectedTrue(d, r2, 40), "true cards, when half is closed");
        }
    }
    
    @DataProvider(name = "oneIndexDecks")
    Object[][] oneIndexDecks() {
        Random rnd = new Random(212_85_06);
        int small[] = new int[52]; // 2,3,4,5,6
        int med[] = new int[52];   // 7,8,9
        int high[] = new int[52];  // T,A
        for (int i = 0; i < 52; i++) {
            int suite = rnd.nextInt(4);
            int sR = 1 + rnd.nextInt(5);
            int mR = 6 + rnd.nextInt(3);
            int hR = 9 + rnd.nextInt(5);
            small[i] = sR + suite*13;
            med[i] =   mR + suite*13;
            high[i] =  (hR + suite*13) % 52; // ace = 13 
        }
        Object[][] data = {
            {small, 1},
            {med, 0},
            {high, -1},
        };
        return data;
    }
    
    private static float expectedTrue(int decks, int running, int cards) {        
        System.out.println(Math.round(running * 5200f / (52*decks - cards)) / 100f);
        return Math.round(running * 5200f / (52*decks - cards)) / 100f;
    }
}
