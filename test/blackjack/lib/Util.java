/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.lib;

import blackjack.fx.Main;
import blackjack.game.Game;
import blackjack.game.Hand;
import blackjack.model.Card;
import blackjack.model.DealAction;
import blackjack.model.GameModel;
import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;

/**
 *
 * @author Fa
 */
public class Util {
    public static Card card(String str) {
        if (str.startsWith("x")) {
            return new Card(Card.rs2id(str.substring(1))).setOpen(false);
        } else {
            return new Card(Card.rs2id(str));
        }
        
    }

    public static Hand hand(String str) {
        Hand hand = new Hand();
        for (String rs: str.split(" ")) {
            hand.add(card(rs.trim()));
        }
        return hand;
    }
    
    public static List<Hand> hands(String str) {
        List<Hand> list = new ArrayList<>();
        for (String h: str.split(";")) {
            list.add(hand(h.trim()));
        }
        return list;
    }
        
    public static GameModel game(String deck) {
        Game game = new Game();
        List<Integer> alterCards = new ArrayList<>();
        for (String rs: deck.split(" ")) {
            alterCards.add(Card.rs2id(rs.trim()));
        }
        int[] alterDeck = new int[alterCards.size()];
        for (int i = 0; i < alterDeck.length; i++) {
            alterDeck[i] = alterCards.get(i);
        }
        game.getShoe().setShuffledCards(alterDeck);
        return game;
    }
    
    public static void play(GameModel model, String action) {
        System.out.println(">> " + action);
        a2r(model,action).run();
    }

    public static void runLater(GameModel model, String action) {
        Platform.runLater(a2r(model, action));        
    }

    private static Runnable a2r(GameModel model, String action) {
        switch (action.trim().toLowerCase()) {
            case "begin"    : return model::begin;
            case "end"      : return model::end;
            case "setmodel" : return () ->  Main.controller.setModel(model);
            case "sh"       : return () -> model.userShuffle();
            case "b50"      : return () -> model.userBet(50, 1);
            case "b50,2"    : return () -> model.userBet(50, 2);
            case "b50,5"    : return () -> model.userBet(50, 5);
            case "b100"     : return () -> model.userBet(100, 1);
            case "h"        : return () -> model.userDealAction(DealAction.HIT);
            case "d"        : return () -> model.userDealAction(DealAction.DOUBLE);
            case "st"       : return () -> model.userDealAction(DealAction.STAND);
            case "sp"       : return () -> model.userDealAction(DealAction.SPLIT);
            default         : return () -> {throw new Error("unknown action " + action);};
        }        
        
    }
}
