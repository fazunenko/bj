/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.game;

import blackjack.model.GameRules;
import blackjack.model.GameRules.Group;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Fa
 */
public class RulesNGTest {
    
    public RulesNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of list method, of class Rules.
     */
    @Test
    public void testList() {
        System.out.println("list");
        GameRules r = new Game().getRules();
        assertEquals(r.list(Group.AFFECT_STRATEGY).size(), 4);
        assertEquals(r.list(Group.EDITABLE).size(), 4);
        assertEquals(r.list(Group.MILLION).size(), 6);
        assertEquals(r.list(Group.RULE).size(), 8);
        assertEquals(r.list(Group.STRATEGY_MENU).size(), 2);
        assertEquals(r.list(Group.SURRENDER).size(), 1);        
        
    }

    @Test
    public void testAsString() {
        System.out.println("asString");
        GameRules r = new Game().getRules();
        assertEquals(r.asString(), "Decks#='#decks 2'; Soft17='Soft17: dealer hits'; Doubling='Double: any two cards'; DoubleAfter='Double after split: allowed'; DealerBJ='Dealer BJ: peek'; Sur='No surrender'; BJ='BJ pays: 3 to 2'; Surrender='No surrender'; Insurance='Suggest insurance'; Strategy='With indices'; BetMethod='Bet pro rata';");
    }

    /**
     * Test of fromString method, of class Rules.
     */
    @Test
    public void testFromString() {
        System.out.println("fromString");
        GameRules r = new Game().getRules();
        r.fromString("Decks#='#decks 6'");        
        assertEquals(r.asString(), "Decks#='#decks 6'; Soft17='Soft17: dealer hits'; Doubling='Double: any two cards'; DoubleAfter='Double after split: allowed'; DealerBJ='Dealer BJ: peek'; Sur='No surrender'; BJ='BJ pays: 3 to 2'; Surrender='No surrender'; Insurance='Suggest insurance'; Strategy='With indices'; BetMethod='Bet pro rata';");
        r.fromString("DealerBJ='Dealer BJ: no peek'; Surrender='Allow surrender'");
        assertEquals(r.asString(), "Decks#='#decks 6'; Soft17='Soft17: dealer hits'; Doubling='Double: any two cards'; DoubleAfter='Double after split: allowed'; DealerBJ='Dealer BJ: no peek'; Sur='Early surrender'; BJ='BJ pays: 3 to 2'; Surrender='Allow surrender'; Insurance='Suggest insurance'; Strategy='With indices'; BetMethod='Bet pro rata';");
        r.fromString(" Insurance='Reject insurance'; Strategy='Basic'; BetMethod='Bet minimum';");        
        assertEquals(r.asString(), "Decks#='#decks 6'; Soft17='Soft17: dealer hits'; Doubling='Double: any two cards'; DoubleAfter='Double after split: allowed'; DealerBJ='Dealer BJ: no peek'; Sur='Early surrender'; BJ='BJ pays: 3 to 2'; Surrender='Allow surrender'; Insurance='Reject insurance'; Strategy='Basic'; BetMethod='Bet minimum';");
        r.fromString("Decks#='#decks 2'; BetMethod='Bet maximum';");        
        assertEquals(r.asString(), "Decks#='#decks 2'; Soft17='Soft17: dealer hits'; Doubling='Double: any two cards'; DoubleAfter='Double after split: allowed'; DealerBJ='Dealer BJ: no peek'; Sur='Early surrender'; BJ='BJ pays: 3 to 2'; Surrender='Allow surrender'; Insurance='Reject insurance'; Strategy='Basic'; BetMethod='Bet maximum';");
    }
    
}
