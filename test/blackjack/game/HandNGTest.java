/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.game;

import blackjack.lib.Util;
import blackjack.model.DealAction;
import blackjack.model.Strategy;
import blackjack.model.Strategy.Act;
import blackjack.model.Strategy.Dealer;
import blackjack.model.Strategy.Player;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author Fa
 */
public class HandNGTest {
    
            
    public HandNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    @Test(dataProvider = "handsAndPoints")
    void testPoints(Hand h, int points) {
        assertEquals(h.points(), points, points + " = " + h);
        if (points < 21) {
            assertFalse(h.isFinal(), "not final " + h + "??? " + h.isFinal()); 
        } else {
            assertTrue(h.isFinal(), "final " + h);
        }
    }
    
    @DataProvider(name = "handsAndPoints")
    public Object[][] handsAndPoints() {
        String[] hands = {
            "",       // skip
            "",       // skip
            "2h",     // 2
            "3s",     // 3
            "2h 2h", // 4
            "2s 3h",
            "2h 2h 2h",
            "2h 2s 3h",
            "2h 2h 2h 2h",
            "3s 3s 3d",
            "Qh",
            "As",
            "As As",
            "As Ac As",
            "As Ad Ah Ac",
            "Kd 2s 3h",
            "Qh 3h 2s As",
            "As 2h 3s As Jc",
            "Td 8s",
            "8h As",
            "Kd Qh",
            "Kd Qh As",
            "Kd Qh 2h",
            "2d 3h 4d 2s 9d 3d"
        };
        Object[][] data = new Object[hands.length - 2][];
        for (int i = 2; i < hands.length; i++) {
            data[i - 2] = new Object[] {Util.hand(hands[i]), i};
        }
        return data;
    }

    @Test(dataProvider = "handsAndSoftPoints")
    void testSoftPoints(String str, int softPoints) {
        Hand h = Util.hand(str);
        assertEquals(h.softPoints(), softPoints, softPoints + " = " + h);
        if (softPoints < 21) {
            assertFalse(h.isFinal(), "not final " + h + "??? " + h.isFinal()); 
        } else {
            assertTrue(h.isFinal(), "final " + h);
        }
    }

    @DataProvider(name = "handsAndSoftPoints")
    public Object[][] handsAndSoftPoints() {
        Object[][] data = {
            {"As", 1},
            {"As Ad", 2},
            {"As 2d", 3},
            {"As 9d", 10},
            {"As As As As As", 5},
            {"As As As 5d As As", 10},
            {"5d As As As As As", 10},
            {"5d As 2s 9c", 17},
            {"Ad 5d 5d", 21},
            {"Ad Qd", 21},
            {"As 7c 8d 9c", 25},
            {"5d", 5},
            {"5d 7c ", 12},
            {"Qc Qs ", 20},
        };
        return data;
    }

    
    @Test
    public void testIsSplit() {
        System.out.println("--- test split negative");
        String yes = "2h 2d ; 2s 2s ; Ac As ; Ad Ad ; Ts Qs ; Ts Kd";
        String no = "2h 3h ; Js As"; 
        for (Hand h: Util.hands(yes)) {
            assertTrue(h.isSplitable(), "split: " + h);
        }
        for (Hand h: Util.hands(no)) {
            assertFalse(h.isSplitable(), "non-split: " + h);
        }
    }

    @Test
    public void testIsBlackJack() {
        System.out.println("--- test blackjack negative");
        String yes = "Ac Qd ; Ks Ah ; Th Ad";
        String no = "As As ; Ts Qs ; 2s Js As ; As Kd Ah ; 5d 5d As ; Qs Qs Ah"; 
        for (Hand h: Util.hands(yes)) {
            assertTrue(h.isBlackJack(), "bj: " + h);
        }
        
        for (Hand h: Util.hands(yes)) {            
            h.setHandObtainedFromSplit(true);
            assertFalse(h.isBlackJack(), "split bj: " + h);
        }
        
        for (Hand h: Util.hands(no)) {
            assertFalse(h.isBlackJack(), "non-bj: " + h);
        }
    }

    @Test
    public void testIsBlackJackClosed() {
        System.out.println("--- test blackjack negative (closed)");
        
        Hand[] blackJackHands = {
            new Hand().add(Util.card("As")).add(Util.card("Kh").setOpen(false)),
            new Hand().add(Util.card("Kc")).add(Util.card("Ad").setOpen(false)),
        };
        
        Hand[] notBlackJackHands = {
            new Hand().add(Util.card("As")).add(Util.card("As").setOpen(false)),
            new Hand().add(Util.card("Ks")).add(Util.card("Kd").setOpen(false)),
        };
        
        for (Hand h: blackJackHands) {
            assertTrue(h.isBlackJack(), "blackjack: " + h);
            int points = h.cards().get(0).points;
            assertEquals(h.points(), points, "points 21"  + h);
            h.openClosedCards();
            assertTrue(h.isBlackJack(), "blackjack: " + h);
            assertEquals(h.points(), 21, "points 21"  + h);
        }

        for (Hand h: notBlackJackHands) {
            assertFalse(h.isBlackJack(), "not blackjack: " + h);            
            assertEquals(h.points(), h.cards().get(0).points, "closed points"  + h);
            h.openClosedCards();
            assertFalse(h.isBlackJack(), "not blackjack: " + h);
            int p = h.cards().get(0).points + h.cards().get(1).points;
            if (p > 20) {
                p = p - 10;
            }
            assertEquals(h.points(), p, "open points"  + h);
        }                
    }
    
    @Test
    public void testAddRemove() {
        System.out.println("--- test add remove");
        Hand h = Util.hand("As");
        multiCheck(h, 11, 1, 1, false, false);
        h.add(Util.card("Ad").setOpen(false));
        multiCheck(h, 11, 11, 2, false, false);
        h.openClosedCards();
        multiCheck(h, 12, 2, 2, false, true);
        h.add(Util.card("2c"));
        multiCheck(h, 14, 4, 3, false, false);
        h.add(Util.card("8c"));
        multiCheck(h, 12, 12, 4, false, false);
        h.add(Util.card("9s"));
        multiCheck(h, 21, 21, 5, true, false);
        
    }
    
    void multiCheck(Hand h, int p, int s, int size, boolean isFinal, boolean isSplit) {
        assertEquals(h.points(), p, "check points " + h.toString());
        assertEquals(h.softPoints(), s, "check soft points " + h.toString());
        assertEquals(h.cards().size(), size, "size " +  h.toString());
        assertEquals(h.isFinal(), isFinal, "isFinal " + h.toString());
        assertEquals(h.isSplitable(), isSplit, "isSplit " + h.toString());
    }
    
    @Test(dataProvider = "handsAndPlayerStates")
    void testPlayerState(String hand, Strategy.Player player) {
        Hand h = Util.hand(hand);
        assertEquals(h.playerState(), player, "Player state " + h);
    }

    @DataProvider(name = "handsAndPlayerStates")
    public Object[][] handsAndPlayerStates() {
        Object[][] data = {
            {"2s 3s", Player.H7},
            {"3s 2s", Player.H7},
            {"2s Jc", Player.H12},
            {"Kd 3s", Player.H13},
            {"2s 2s 2d Ts Ad", Player.H17},
            {"2s 3s 4d Ts Ad", Player.H18},
            {"2s 3s 4d Ts Ad", Player.H18},
            {"Ad Ad Ad Ad Ad Ad Ad Ad Ad Ad Ad Ad", Player.H12},
            
            {"2s 2s", Player.P2},
            {"Js Jc", Player.PT},
            {"Ks Td", Player.PT},
            {"Js Qd", Player.PT},
            {"As Ah", Player.PA},

            {"Ac 2s", Player.A2},
            {"2c 2d As", Player.A4},
            {"5c Ah 4d", Player.A9},
            {"Ad Ad Ad Ad Ad Ad Ad Ad Ad", Player.A8},
        };
        return data;
    }

    @Test(dataProvider = "handsAndDealerStates")
    void testDealerState(String hand, Strategy.Dealer dealer) {
        Hand h = Util.hand(hand);
        assertEquals(h.dealerState(), dealer, "Dealer state " + h);        
    }
    
    @DataProvider(name = "handsAndDealerStates")
    public Object[][] handsAndDealerStates() {
        Object[][] data = {
            {"2s x3s", Dealer.D2},
            {"9s xAs", Dealer.D9},
            {"xAs Jh", Dealer.DT},
            {"Qs x3h", Dealer.DT},
            {"Kd xKs", Dealer.DT},
            {"Ad x8s", Dealer.DA},
        };
        return data;
    }


    @Test(dataProvider = "handsActions")
    void testAction(String hand, DealAction[] dacts) {
        Hand h = Util.hand(hand);
        for (int i = 0; i < Strategy.Act.values().length; i++) {
            Act a = Strategy.Act.values()[i];
            DealAction expected = dacts[i];
            DealAction returned;
            try {
                returned = h.action(a);
            } catch (Error e) {
                returned = null;
            }
            assertEquals(returned, expected, "Action test. Hand " + h + " act " + a);
        }
    }
    
    @DataProvider(name = "handsActions")
    public Object[][] handsActions() {
        //         Hit, Stand, Split, Double, DoubleStand
        Object[][] data = {
            {"2s 2s", new DealAction[] {DealAction.HIT, DealAction.STAND, DealAction.SPLIT, DealAction.DOUBLE, DealAction.DOUBLE, DealAction.HIT, DealAction.STAND}},
            {"As As", new DealAction[] {DealAction.HIT, DealAction.STAND, DealAction.SPLIT, DealAction.DOUBLE, DealAction.DOUBLE, DealAction.HIT, DealAction.STAND}},
            {"2s As", new DealAction[] {DealAction.HIT, DealAction.STAND, DealAction.SPLIT, DealAction.DOUBLE, DealAction.DOUBLE, DealAction.HIT, DealAction.STAND}},
            {"3s Js", new DealAction[] {DealAction.HIT, DealAction.STAND, DealAction.SPLIT, DealAction.DOUBLE, DealAction.DOUBLE, DealAction.HIT, DealAction.STAND}},
            
            {"3s Js Ad",    new DealAction[] {DealAction.HIT, DealAction.STAND, DealAction.SPLIT, DealAction.HIT, DealAction.STAND, DealAction.HIT, DealAction.STAND}},
            {"2s 2s 2d",    new DealAction[] {DealAction.HIT, DealAction.STAND, DealAction.SPLIT, DealAction.HIT, DealAction.STAND, DealAction.HIT, DealAction.STAND}},
            {"As Ac Ad Ah", new DealAction[] {DealAction.HIT, DealAction.STAND, DealAction.SPLIT, DealAction.HIT, DealAction.STAND, DealAction.HIT, DealAction.STAND}},
            
        };
        return data;
    }
    
}
