/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.game;

import blackjack.lib.Util;
import blackjack.fx.Main;
import blackjack.model.DealModel;
import blackjack.model.GameModel;
import blackjack.model.GameState;
import blackjack.model.HandModel;
import javafx.collections.ObservableList;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author Fa
 */
public class GameNGTest {
    
    public GameNGTest() {
    }
        

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
        
    @DataProvider(name = "positiveScenarios")
    Object[][] positiveScenarios() {
        PositiveScenario[] scenarios = {
            new PositiveScenario(
                    "x",
                    "Ah 8d 2h 7d Td 6d 2s As Ac Ah",
                    "Sh b50 h h st",
                    new State(GameState.BET, -50, 17, 19)
            ),

            new PositiveScenario(
                    "y",
                    "3h 2h 5d Ad 6d 6d Ts As Ac Ah",
                    "Sh b100 d",
                    new State(GameState.BET, +200, 19, 14)
            ),
            new PositiveScenario(
                    "z",
                    "5h 2d 5h Ad Ad 6d Ts As Ac 3h",
                    "Sh b100 sp st d",
                    new State(GameState.BET, -100, 18, 16, 21)
            )

        };
        Object[][] result = new Object[scenarios.length][];
        for (int i = 0; i < scenarios.length; i++) {
            result[i] = new Object[] {scenarios[i], null};
        }
        return result;
    }

    /**
     * Test of getGameState method, of class Game.
     */
    @Test(dataProvider="positiveScenarios")
    void testGetGameState(PositiveScenario ps, Object ignore) {
        System.out.println("---- testing " + ps.descr);
        GameModel model = Util.game(ps.deck);       
        model.begin();
        for (String a: ps.actions.split(" ")) {
            Util.play(model, a);
        }
        check(model, ps.state);        
    }
    
    
    void check(GameModel model, State state) {
        assertEquals(model.getGameState().get(), state.gstate, "game state");
        DealModel deal = model.getDealModel();
        assertEquals(deal.dealerHand().result().get(), state.summary, "deal result");
        assertEquals(deal.dealerHand().points(), state.dealer, "dealer points");
        ObservableList<? extends HandModel> players = deal.playerHands();
        assertEquals(players.size(), state.player.length, "number of player hands");
        for (int i = 0; i < players.size(); i++) {
            assertEquals(players.get(i).points(), state.player[i], "points of hand #" + i + " " + players.get(i));
        }
    }
    
    static class PositiveScenario {
        final String descr;
        final String deck;
        final String actions;
        final State state;
        PositiveScenario(String descr, String deck, String actions, State state) {
            this.descr = descr;
            this.deck = deck;
            this.actions = actions;
            this.state = state;
        }
    }
    
    static class State {
        final GameState gstate;
        final int summary;
        final int dealer;
        final int[] player;
        State(GameState gstate, int summary, int dealer, int... player) {
            this.gstate = gstate;
            this.summary = summary;
            this.dealer = dealer;
            this.player = player;
        }
    }       
}
