/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.game;

import java.io.File;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author Fa
 */
public class FlightRecordingTestNGTest {
    
    public FlightRecordingTestNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @DataProvider(name = "frFileProver")
    Object[][] frFileProver() {
        Path dir = Paths.get("test/blackjack/game");
        File[] frFiles = dir.toFile().listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File directory, String fileName) {
                if (fileName.endsWith(".fr")) {
                    return true;
                }
                return false;
            }
        });

        Object[][] result = new Object[frFiles.length][];
        for (int i = 0; i < frFiles.length; i++) {
            result[i] = new Object[] {frFiles[i], null};
        }
        return result;
    }
    
    /**
     * Test which play a given Flight Recording file.
     */
    @Test(dataProvider="frFileProver")
    void testGetGameState(File frFile, Object ignore) {
        System.out.println("---- testing " + frFile);
        List<String> ref = null;
        List<String> out = null;
        File tmp = null;
        try {
            ref = Files.readAllLines(Paths.get(frFile.getPath()));
            String rules = ref.get(0).substring(FlightRecorder.ON.RULE_PREFIX.length());
            String deck = ref.get(1).substring(FlightRecorder.ON.BEFORE_PR.length());

            Game g = new Game();
            g.getShoe().setShuffledCards(deck);
            g.getRules().fromString(rules);
            tmp = File.createTempFile("tmpBJ_", ".fr");
            g.setRecording(tmp);
            g.begin();
            Game.playShuffle(g, 1);
            out = Files.readAllLines(Paths.get(tmp.getPath()));            
        } catch (Exception e) {
            e.printStackTrace();
            throw new Error(e);
        }               
        if (!compare(out, ref)) {
            printfile("Expected", ref);
            printfile("In fact", out);
            Assert.fail("Files differ: ref: " + frFile.getAbsolutePath() +"; out: " + tmp.getAbsolutePath());
        } else {
            tmp.deleteOnExit();
        }
        System.out.println("-- passed");
    }

    /**
     * Compares two given list of strings for being equal.
     * null are not expected to be given.
     * @param list1
     * @param list2
     * @return true iff both lists contains the same lines
     */    
    private boolean compare(List<String> list1, List<String> list2) {
        if (list1.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list1.size(); i++) {
            if (!list1.get(i).equals(list2.get(i))) {
                return false;
            }
        }
        return true;
    }
    
    private void printfile(String msg, List<String> list) {
        System.out.println("vvvvvvvvvv " + msg + " vvvvvvvvvv");
        for (String s: list) {
            System.out.println(s);
        }
        System.out.println("^^^^^^^^^^ " + msg + " ^^^^^^^^^^");
    }
        
}
