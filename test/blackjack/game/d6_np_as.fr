RULES: Decks#='#decks 6'; Soft17='Soft17: dealer hits'; Doubling='Double: any two cards'; DoubleAfter='Double after split: allowed'; DealerBJ='Dealer BJ: no peek'; Sur='Early surrender'; BJ='BJ pays: 3 to 2'; Surrender='Allow surrender'; Insurance='Suggest insurance'; Strategy='With indices'; BetMethod='Bet pro rata';
SHOE_BEFORE_SHUFFLE: Ac Qh Ts Jc Ah 5c Kc 6h 3s Ac 6d 8c Jc 8c 7c 4c Qh 3h 8s Ac 2h 6d 5s 3s 7s 5c 6c 4d 2s 5s Ah 4s 7h 2s 7h 6s 3c As Qd 7c 8s Ac 5d Ts Qh 9s Tc 4s Ac Ks 9s As 2c 4s 8s 6s Qh 5s 8c 9c 4d 3d 6d 5h Ah 3c 6s Jd 8s Kd 3h 5d 9s Ad 3s Jc 2h As 9h Qs Qd 2d 2h Qs Qd Qs 9s Jh Jd 4c Qs Jc 2d Ts 8h Jh Kd 4c Kc 8h Qd Kc Kd 6d 9d 6s Qd 2c 4d 7c 3s 3c Js Th Ah 4c 5d Jd 6c 5c 3d 4h Tc 5h 7s Td 8d 6s Th 9c Kh 6c 7d 3d Ts 6h 9d 4h 8d 3h Qs 6h 5c As 6s 5h 8c 9c Jd Ks 3h 2h Ad 6h 3s 5h Jd Qc Ad 7d Ks Ad Js Ks 9h Jd 7h 9d Kc 9s 6h 2c Td Qh 9s 8h Ts Ad Ad 8h 3h 5s 6c 9h 4d Th 8d As Jh 2d 7c Td 8h 6c Ah 2c Qc 7s Js Ah 8d 2h 2d 5s 9d Kh 4d 5c Qd 8h Ac 9c Th 2h 4s Qh Tc Ks 5d 2s Kd Jh 4h Th 9h Jh 3c Qc 7s Qc 5d 7s 4h 2s 3c 9h 4c 9d 4h 6d Ks Jh Kc 3c Qc 7h 8s 4c 7d 7d Kd 7d Js 5h 6d Ts 9c Qs 3d Kh 7d 8d 4d 8c 5s 4s Jc 3h Tc Qc Kh 7s Td Tc 5h As 2c Jc Kh Td 3d 2d 9d 8d Js 6h Kh 2d 4h 8s Kd 5d Kc 2c 6c 3s Td 3d 7h 8c 7c Th 2s Tc 5c 9c Js 7c 9h 7h 4s 2s
  BET_50_1; rc = -4
  BET_50_1; rc = -4
  BET_50_1; rc = -3
    Insurance: no; total=150; rc = -3
    Hit; total=150; rc = -4
    Stand; total=150; rc = -4
  BET_50_1; rc = -3
    Hit; total=150; rc = -2
    Hit; total=100; rc = -3
  BET_50_1; rc = -2
    Hit; total=100; rc = -1
    Stand; total=150; rc = 1
  BET_50_1; rc = 4
    Hit; total=150; rc = 3
    Stand; total=100; rc = 5
  BET_50_1; rc = 7
    Hit; total=100; rc = 6
    Stand; total=50; rc = 6
  BET_50_1; rc = 5
    Stand; total=100; rc = 5
  BET_50_1; rc = 3
    Stand; total=150; rc = 2
  BET_50_1; rc = 2
    Insurance: no; total=150; rc = 2
    Hit; total=150; rc = 2
    Stand; total=100; rc = 4
  BET_50_1; rc = 4
    Stand; total=100; rc = 5
  BET_50_1; rc = 8
    Hit; total=100; rc = 9
    Hit; total=100; rc = 10
    Stand; total=100; rc = 8
  BET_50_1; rc = 8
    Hit; total=100; rc = 8
    Stand; total=150; rc = 9
  BET_50_1; rc = 8
  BET_50_1; rc = 7
    Hit; total=175; rc = 7
  BET_50_1; rc = 5
    Stand; total=125; rc = 4
  BET_50_1; rc = 3
    Stand; total=175; rc = 2
  BET_50_1; rc = 0
    Stand; total=225; rc = 0
  BET_50_1; rc = -2
    Stand; total=175; rc = -3
  BET_50_1; rc = -1
    Hit; total=175; rc = 0
    Hit; total=175; rc = 1
    Stand; total=125; rc = 0
  BET_50_1; rc = 2
    Double; total=225; rc = 0
  BET_50_1; rc = 1
    Hit; total=225; rc = 2
    Hit; total=225; rc = 3
    Stand; total=275; rc = 3
  BET_50_1; rc = 3
    Hit; total=325; rc = 3
  BET_50_1; rc = 3
    Surrender; total=300; rc = 3
  BET_50_1; rc = 4
    Hit; total=300; rc = 5
    Hit; total=350; rc = 5
  BET_50_1; rc = 6
    Hit; total=350; rc = 5
    Stand; total=300; rc = 7
  BET_100_1; rc = 8
    Hit; total=200; rc = 6
  BET_50_1; rc = 7
    Hit; total=200; rc = 8
    Hit; total=200; rc = 9
    Hit; total=150; rc = 9
SHOE_AFTER_SHUFFLE: Qc Ad 7d Ks Ad Js Ks 9h Jd 7h 9d Kc 9s 6h 2c Td Qh 9s 8h Ts Ad Ad 8h 3h 5s 6c 9h 4d Th 8d As Jh 2d 7c Td 8h 6c Ah 2c Qc 7s Js Ah 8d 2h 2d 5s 9d Kh 4d 5c Qd 8h Ac 9c Th 2h 4s Qh Tc Ks 5d 2s Kd Jh 4h Th 9h Jh 3c Qc 7s Qc 5d 7s 4h 2s 3c 9h 4c 9d 4h 6d Ks Jh Kc 3c Qc 7h 8s 4c 7d 7d Kd 7d Js 5h 6d Ts 9c Qs 3d Kh 7d 8d 4d 8c 5s 4s Jc 3h Tc Qc Kh 7s Td Tc 5h As 2c Jc Kh Td 3d 2d 9d 8d Js 6h Kh 2d 4h 8s Kd 5d Kc 2c 6c 3s Td 3d 7h 8c 7c Th 2s Tc 5c 9c Js 7c 9h 7h 4s 2s
