/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack.game;

import blackjack.model.Strategy;
import blackjack.model.Strategy.Act;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Fa
 */
public class TableStrategyNGTest {
    
    public TableStrategyNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of action method, of class TableStrategy.
     */
    @Test
    public void testAction() {                
        Object[][] strategies = {
            {Strategies.BASIC_STRATEGY, Act.Double, Act.Hit, Act.Stand},
            {Strategies.NO_PEEK_NO_SURRENDER_STRATEGY, Act.Hit, Act.Hit, Act.Stand},
            {Strategies.NO_PEEK_SURRENDER_STRATEGY, Act.Hit, Act.Surrender, Act.SurrenderStand},
            {Strategies.PEEK_SURRENDER_STRATEGY, Act.Double, Act.Surrender, Act.SurrenderStand}
        };
        
        for (int i = 0; i < strategies.length; i++) {
            Strategy s = (Strategy) strategies[i][0];
            System.out.println("Strategy: " + s);
            Assert.assertEquals(s.actions(Strategy.Dealer.DT, Strategy.Player.H11).choose(0), (Act) strategies[i][1]);
            Assert.assertEquals(s.actions(Strategy.Dealer.D9, Strategy.Player.H16).choose(0), (Act) strategies[i][2]);
            Assert.assertEquals(s.actions(Strategy.Dealer.DA, Strategy.Player.H17).choose(0), (Act) strategies[i][3]);
        }
        
    }
    
}
